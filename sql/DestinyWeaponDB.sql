CREATE DATABASE IF NOT EXISTS DESTINYWEAPON;
USE DESTINYWEAPON;

drop table if exists barrel, optic, perkOnWeapons, perk, crucibleStat, baseStat, rocketLauncherStat, weapon, rarity, weaponType, weaponSlot;

create table weaponSlot (
	slotName varchar(50) check(slotName in ('Primary', 'Special', 'Heavy', 'Primary-Special')),
	synthesisGlimmerCost integer(5),
	primary key (slotName)
);

create table weaponType (
	typeName varchar(50),
	generalRange varchar(10) check(generalRange in('Long', 'Mid', 'Short', 'Mid-Long', 'Short-Mid')),
	generalAccuracy varchar(10) check(generalAccuracy in('High', 'Medium', 'Low')),
	generalDamage varchar(10) check(generalDamage in('High', 'Medium', 'Low')),
	firingType varchar(10) check(firingType in('Single', 'Auto', 'Charge')),
	primary key (typeName)
);

create table rarity (
  rarityName varchar(50) check(rarityName in ('Basic', 'Uncommon', 'Rare', 'Legendary', 'Exotic')),
  color varchar(50) check(color in ('White', 'Green', 'Blue', 'Purple', 'Yellow')),
  numPerks integer(5),
  numDamageUpgrades integer(5),
  primary key (rarityName)
);

create table weapon (
  weaponName varchar(50) not null,
  weaponYear integer(1) not null,
  source varchar(50),
  weaponTypeName varchar(50) not null,
  weaponSlotName varchar(50) not null check(weaponSlotName in('Primary', 'Special', 'Heavy', 'Primary-Special')),
  weaponRarity varchar(50) not null check(weaponRarity in('Basic', 'Uncommon', 'Rare', 'Legendary', 'Exotic')),
  imgPath varchar(100),
  primary key (weaponName, weaponYear),
  foreign key (weaponTypeName) references weaponType(typeName),
  foreign key (weaponSlotName) references weaponSlot(slotName),
  foreign key (weaponRarity) references rarity(rarityName),
  index (weaponName)
);

create table rocketLauncherStat (
	weaponName varchar(50) not null,
	velocity integer(5),
	blastRadius integer(5),
  stability integer(5),
	capacity integer(5),
  reload integer(5),
  aimassist integer(5),
	primary key (weaponName),
	foreign key (weaponName) references weapon(weaponName)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
	index (weaponName)
);

create table baseStat (
  weaponName varchar(50) not null,
  weaponRange integer(5),
  stability integer(5),
  impact integer(5),
  reload integer(5),
  primary key (weaponName),
  foreign key (weaponName) references weapon(weaponName)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  index (weaponName)
);

create table crucibleStat (
   weaponName varchar(50) not null,
   bodyTTK decimal(10,2),
   critTTK decimal(10,2),
	 aimAssist integer(5),
	 primary key (weaponName),
   foreign key (weaponName) references weapon(weaponName)
     ON DELETE CASCADE
     ON UPDATE CASCADE,
  index (weaponName)
);

create table perk (
	perkName varchar(50) not null,
	description varchar(100),
	perkType varchar(10) check(perkType in('Optic', 'Barrel', 'Ability')),
	primary key(perkName),
	index(perkName)
);

create table perkOnWeapons (
	perkName VARCHAR(50),
	weaponTypeName varchar(50),
	foreign key (perkName) references perk(perkName) ON DELETE CASCADE
);

create table optic (
	opticName varchar(50) not null,
	zoomRate integer(5),
	reloadMod integer(5),
	rangeMod integer(5),
	stabilityMod integer(5),
	handlingMod integer(5),
	aimAssistMod integer(5),
	primary key (opticName),
	foreign key (opticName) references perk(perkName) ON DELETE CASCADE
);

create table barrel (
	barrelName varchar(50) not null,
	damageMod integer(5),
	rangeMod integer(5),
	stabilityMod integer(5),
	aimAssistMod integer(5),
	primary key (barrelName),
	foreign key (barrelName) references perk(perkName) ON DELETE CASCADE
);

INSERT INTO weaponSlot VALUES 
('Primary', 100),
('Special', 250),
('Heavy', 950),
('Primary-Special', 250);

INSERT INTO weaponType VALUES
('Auto Rifle', 'Short-Mid', 'Medium', 'Low', 'Auto'),
('Scout Rifle', 'Mid-Long', 'High', 'Medium', 'Single'),
('Pulse Rifle', 'Mid-Long', 'Medium', 'Low', 'Single'),
('Hand Cannon', 'Mid-Long', 'High', 'High', 'Single'),
('Shotgun', 'Short', 'Low', 'High', 'Single'),
('Sniper Rifle', 'Long', 'High', 'High', 'Single'),
('Fusion Rifle', 'Short-Mid', 'Medium', 'High', 'Charge'),
('Sidearm', 'Short-Mid', 'Medium', 'Low', 'Single'),
('Machine Gun', 'Mid', 'Low', 'High', 'Auto'),
('Rocket Launcher', 'Long', 'Low', 'High', 'Single'),
('Sword', 'Short', 'High', 'High', 'Single');

INSERT INTO rarity VALUES
('Basic', 'White', 0, 0),
('Uncommon', 'Green', 1, 1),
('Rare', 'Blue', 2, 2),
('Legendary', 'Purple', 3, 5),
('Exotic', 'Yellow', 4, 5);

INSERT INTO weapon VALUES ('Vex Mythoclast', '1', 'VoG', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Vex%20Mythoclast.jpg');
INSERT INTO weapon VALUES ('An Answering Chord', '2', 'Vanguard Qtr', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/An%20Answering%20Chord.jpg');
INSERT INTO weapon VALUES ('SUROS ARI-45', '2', 'Suros', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/SUROS%20ARI-45.jpg');
INSERT INTO weapon VALUES ('Shadow Price', '1', 'Vanguard', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Shadow%20Price.jpg');
INSERT INTO weapon VALUES ('Her Vengeance', '1', 'Q''s Wrath', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Her%20Vengeance.jpg');
INSERT INTO weapon VALUES ('The Summoner', '1', 'Trials', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/The%20Summoner.jpg');
INSERT INTO weapon VALUES ('Vanquisher VIII', '1', 'N Monarchy', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Vanquisher%20VIII.jpg');
INSERT INTO weapon VALUES ('Doctrine of Passing', '2', 'Trials', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Doctrine%20of%20Passing.jpg');
INSERT INTO weapon VALUES ('Wolfslayer''s Claw', '1', 'Varik', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Wolfslayer''s%20Claw.jpg');
INSERT INTO weapon VALUES ('Does Not Bow', '2', 'Strike', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Does%20Not%20Bow.jpg');
INSERT INTO weapon VALUES ('Antipodal Hindsight', '2', 'Crucible Qtr', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Antipodal%20Hindsight.jpg');
INSERT INTO weapon VALUES ('Pest Control Matrix', '1', 'Vanguard', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Pest%20Control%20Matrix.jpg');
INSERT INTO weapon VALUES ('Grim Citizen III', '1', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Grim%20Citizen%20III.jpg');
INSERT INTO weapon VALUES ('Anguish of Drystan', '2', 'King''s Fall', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Anguish%20of%20Drystan.jpg');
INSERT INTO weapon VALUES ('Her Right Hand', '1', 'Q''s Chest', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Her%20Right%20Hand.jpg');
INSERT INTO weapon VALUES ('SUROS Regime', '2', 'Various', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/SUROS%20Regime.jpg');
INSERT INTO weapon VALUES ('The Dealbreaker', '2', 'Crucible Qtr', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/The%20Dealbreaker.jpg');
INSERT INTO weapon VALUES ('Questing Beast', '2', 'Vanguard Qtr', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Questing%20Beast.jpg');
INSERT INTO weapon VALUES ('Dispatch I11', '1', 'HoW', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Dispatch%20I11.jpg');
INSERT INTO weapon VALUES ('Hard Light', '1', 'Various', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Hard%20Light.jpg');
INSERT INTO weapon VALUES ('Silimar''s Wrath', '1', 'Iron Banner', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Silimar''s%20Wrath.jpg');
INSERT INTO weapon VALUES ('Paleocontact JPK-43', '2', 'Dead Orbit', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Paleocontact%20JPK-43.jpg');
INSERT INTO weapon VALUES ('Monte Carlo', '2', 'Various', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Monte%20Carlo.jpg');
INSERT INTO weapon VALUES ('Low Down P-XIV', '1', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Low%20Down%20P-XIV.jpg');
INSERT INTO weapon VALUES ('Zhalo Supercell', '2', 'Various', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Zhalo%20Supercell.jpg');
INSERT INTO weapon VALUES ('Up For Anything', '1', 'Vanguard', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Up%20For%20Anything.jpg');
INSERT INTO weapon VALUES ('Abyss Defiant', '1', 'Crota HM', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Abyss%20Defiant.jpg');
INSERT INTO weapon VALUES ('For The People', '1', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/For%20The%20People.jpg');
INSERT INTO weapon VALUES ('Haakon''s Hatchet', '2', 'Iron Banner', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Haakon''s%20Hatchet.jpg');
INSERT INTO weapon VALUES ('SUROS ARI-41', '2', 'Suros', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/SUROS%20ARI-41.jpg');
INSERT INTO weapon VALUES ('Zarinaea-D', '2', 'Haake', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Zarinaea-D.jpg');
INSERT INTO weapon VALUES ('Do Gooder V', '1', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Do%20Gooder%20V.jpg');
INSERT INTO weapon VALUES ('Righteous VII', '2', 'N Monarchy', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Righteous%20VII.jpg');
INSERT INTO weapon VALUES ('Vindicator XI', '1', 'N Monarchy', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Vindicator%20XI.jpg');
INSERT INTO weapon VALUES ('First Rule DHYB', '1', 'HoW', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/First%20Rule%20DHYB.jpg');
INSERT INTO weapon VALUES ('Fabian Strategy', '2', 'Gunsmith', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Fabian%20Strategy.jpg');
INSERT INTO weapon VALUES ('Red Spectre', '2', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Red%20Spectre.jpg');
INSERT INTO weapon VALUES ('Necrochasm', '1', 'Crota HM', 'Auto Rifle', 'Primary', 'Exotic', '../../images/AutoRifle/Necrochasm.jpg');
INSERT INTO weapon VALUES ('Arminius-D', '2', 'Haake', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Arminius-D.jpg');
INSERT INTO weapon VALUES ('Atheon''s Epilogue', '1', 'VoG', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Atheon''s%20Epilogue.jpg');
INSERT INTO weapon VALUES ('Payback SOS', '1', 'Vanguard', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Payback%20SOS.jpg');
INSERT INTO weapon VALUES ('Doctor Nope', '1', 'Vanguard', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Doctor%20Nope.jpg');
INSERT INTO weapon VALUES ('Eidolon Ally', '1', 'Eris', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Eidolon%20Ally.jpg');
INSERT INTO weapon VALUES ('Hex Caster ARC', '1', 'Crucible', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Hex%20Caster%20ARC.jpg');
INSERT INTO weapon VALUES ('Unwilling Soul-09', '1', 'Dead Orbit', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/Unwilling%20Soul-09.jpg');
INSERT INTO weapon VALUES ('AR760 Truth Serum', '1', 'HoW', 'Auto Rifle', 'Primary', 'Legendary', '../../images/AutoRifle/AR760%20Truth%20Serum.jpg');
INSERT INTO baseStat VALUES ('Vex Mythoclast', '49', '60', '28', '72');
INSERT INTO baseStat VALUES ('An Answering Chord', '54', '32', '28', '63');
INSERT INTO baseStat VALUES ('SUROS ARI-45', '48', '44', '28', '52');
INSERT INTO baseStat VALUES ('Shadow Price', '37', '56', '28', '67');
INSERT INTO baseStat VALUES ('Her Vengeance', '26', '50', '28', '71');
INSERT INTO baseStat VALUES ('The Summoner', '32', '54', '28', '62');
INSERT INTO baseStat VALUES ('Vanquisher VIII', '32', '54', '28', '72');
INSERT INTO baseStat VALUES ('Doctrine of Passing', '32', '54', '28', '62');
INSERT INTO baseStat VALUES ('Wolfslayer''s Claw', '33', '39', '28', '59');
INSERT INTO baseStat VALUES ('Does Not Bow', '33', '39', '28', '59');
INSERT INTO baseStat VALUES ('Antipodal Hindsight', '32', '54', '28', '62');
INSERT INTO baseStat VALUES ('Pest Control Matrix', '28', '46', '28', '59');
INSERT INTO baseStat VALUES ('Grim Citizen III', '42', '39', '28', '65');
INSERT INTO baseStat VALUES ('Anguish of Drystan', '21', '54', '28', '41');
INSERT INTO baseStat VALUES ('Her Right Hand', '23', '49', '28', '66');
INSERT INTO baseStat VALUES ('SUROS Regime', '28', '46', '28', '65');
INSERT INTO baseStat VALUES ('The Dealbreaker', '40', '37', '28', '41');
INSERT INTO baseStat VALUES ('Questing Beast', '17', '41', '28', '59');
INSERT INTO baseStat VALUES ('Dispatch I11', '22', '28', '28', '58');
INSERT INTO baseStat VALUES ('Hard Light', '28', '73', '8', '86');
INSERT INTO baseStat VALUES ('Silimar''s Wrath', '20', '86', '8', '55');
INSERT INTO baseStat VALUES ('Paleocontact JPK-43', '20', '72', '8', '68');
INSERT INTO baseStat VALUES ('Monte Carlo', '46', '64', '8', '79');
INSERT INTO baseStat VALUES ('Low Down P-XIV', '22', '76', '8', '56');
INSERT INTO baseStat VALUES ('Zhalo Supercell', '28', '46', '8', '72');
INSERT INTO baseStat VALUES ('Up For Anything', '27', '70', '8', '59');
INSERT INTO baseStat VALUES ('Abyss Defiant', '25', '44', '8', '85');
INSERT INTO baseStat VALUES ('For The People', '30', '66', '8', '65');
INSERT INTO baseStat VALUES ('Haakon''s Hatchet', '23', '50', '8', '66');
INSERT INTO baseStat VALUES ('SUROS ARI-41', '17', '68', '8', '54');
INSERT INTO baseStat VALUES ('Zarinaea-D', '25', '60', '8', '65');
INSERT INTO baseStat VALUES ('Do Gooder V', '22', '42', '8', '77');
INSERT INTO baseStat VALUES ('Righteous VII', '17', '68', '8', '49');
INSERT INTO baseStat VALUES ('Vindicator XI', '21', '48', '8', '84');
INSERT INTO baseStat VALUES ('First Rule DHYB', '31', '46', '8', '79');
INSERT INTO baseStat VALUES ('Fabian Strategy', '22', '42', '8', '55');
INSERT INTO baseStat VALUES ('Red Spectre', '22', '28', '8', '76');
INSERT INTO baseStat VALUES ('Necrochasm', '19', '64', '2', '76');
INSERT INTO baseStat VALUES ('Arminius-D', '13', '42', '2', '76');
INSERT INTO baseStat VALUES ('Atheon''s Epilogue', '15', '34', '2', '71');
INSERT INTO baseStat VALUES ('Payback SOS', '17', '31', '2', '78');
INSERT INTO baseStat VALUES ('Doctor Nope', '17', '32', '2', '68');
INSERT INTO baseStat VALUES ('Eidolon Ally', '19', '37', '2', '65');
INSERT INTO baseStat VALUES ('Hex Caster ARC', '24', '21', '2', '76');
INSERT INTO baseStat VALUES ('Unwilling Soul-09', '23', '22', '2', '68');
INSERT INTO baseStat VALUES ('AR760 Truth Serum', '22', '28', '2', '65');
INSERT INTO crucibleStat VALUES('Vex Mythoclast', '0.96', '0.64', '100');
INSERT INTO crucibleStat VALUES('An Answering Chord', '1.20', '0.93', '59');
INSERT INTO crucibleStat VALUES('SUROS ARI-45', '1.20', '0.93', '54');
INSERT INTO crucibleStat VALUES('Shadow Price', '1.20', '0.93', '45');
INSERT INTO crucibleStat VALUES('Her Vengeance', '1.20', '0.93', '56');
INSERT INTO crucibleStat VALUES('The Summoner', '1.20', '0.93', '50');
INSERT INTO crucibleStat VALUES('Vanquisher VIII', '1.20', '0.93', '45');
INSERT INTO crucibleStat VALUES('Doctrine of Passing', '1.20', '0.93', '50');
INSERT INTO crucibleStat VALUES('Wolfslayer''s Claw', '1.20', '0.93', '65');
INSERT INTO crucibleStat VALUES('Does Not Bow', '1.20', '0.93', '65');
INSERT INTO crucibleStat VALUES('Antipodal Hindsight', '1.20', '0.93', '45');
INSERT INTO crucibleStat VALUES('Pest Control Matrix', '1.20', '0.93', '55');
INSERT INTO crucibleStat VALUES('Grim Citizen III', '1.20', '0.93', '37');
INSERT INTO crucibleStat VALUES('Anguish of Drystan', '1.20', '0.93', '55');
INSERT INTO crucibleStat VALUES('Her Right Hand', '1.20', '0.93', '44');
INSERT INTO crucibleStat VALUES('SUROS Regime', '1.20', '0.93', '40');
INSERT INTO crucibleStat VALUES('The Dealbreaker', '1.20', '0.93', '36');
INSERT INTO crucibleStat VALUES('Questing Beast', '1.20', '0.93', '40');
INSERT INTO crucibleStat VALUES('Dispatch I11', '1.20', '0.93', '35');
INSERT INTO crucibleStat VALUES('Hard Light', '1.20', '0.90', '90');
INSERT INTO crucibleStat VALUES('Silimar''s Wrath', '1.20', '0.90', '74');
INSERT INTO crucibleStat VALUES('Paleocontact JPK-43', '1.20', '0.90', '74');
INSERT INTO crucibleStat VALUES('Monte Carlo', '1.20', '0.90', '50');
INSERT INTO crucibleStat VALUES('Low Down P-XIV', '1.20', '0.90', '72');
INSERT INTO crucibleStat VALUES('Zhalo Supercell', '1.20', '0.90', '80');
INSERT INTO crucibleStat VALUES('Up For Anything', '1.20', '0.90', '62');
INSERT INTO crucibleStat VALUES('Abyss Defiant', '1.20', '0.90', '75');
INSERT INTO crucibleStat VALUES('For The People', '1.20', '0.90', '57');
INSERT INTO crucibleStat VALUES('Haakon''s Hatchet', '1.20', '0.90', '79');
INSERT INTO crucibleStat VALUES('SUROS ARI-41', '1.20', '0.90', '72');
INSERT INTO crucibleStat VALUES('Zarinaea-D', '1.20', '0.90', '60');
INSERT INTO crucibleStat VALUES('Do Gooder V', '1.20', '0.90', '70');
INSERT INTO crucibleStat VALUES('Righteous VII', '1.20', '0.90', '63');
INSERT INTO crucibleStat VALUES('Vindicator XI', '1.20', '0.90', '61');
INSERT INTO crucibleStat VALUES('First Rule DHYB', '1.20', '0.90', '50');
INSERT INTO crucibleStat VALUES('Fabian Strategy', '1.20', '0.90', '50');
INSERT INTO crucibleStat VALUES('Red Spectre', '1.20', '0.90', '50');
INSERT INTO crucibleStat VALUES('Necrochasm', '0.93/1.00', '0.80', '75');
INSERT INTO crucibleStat VALUES('Arminius-D', '0.93/1.00', '0.80/0.86', '80');
INSERT INTO crucibleStat VALUES('Atheon''s Epilogue', '0.93/1.00', '0.80/0.86', '82');
INSERT INTO crucibleStat VALUES('Payback SOS', '0.93/1.00', '0.80/0.86', '78');
INSERT INTO crucibleStat VALUES('Doctor Nope', '0.93/1.00', '0.80/0.86', '79');
INSERT INTO crucibleStat VALUES('Eidolon Ally', '0.93/1.00', '0.80', '66');
INSERT INTO crucibleStat VALUES('Hex Caster ARC', '0.93/1.00', '0.80/0.86', '67');
INSERT INTO crucibleStat VALUES('Unwilling Soul-09', '0.93/1.00', '0.80/0.86', '68');
INSERT INTO crucibleStat VALUES('AR760 Truth Serum', '0.93/1.00', '0.80/0.86', '25');

INSERT INTO weapon VALUES ('The Jade Rabbit', '2', 'Various', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/The%20Jade%20Rabbit.jpg');
INSERT INTO weapon VALUES ('The Scholar', '1', 'Trials', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/The%20Scholar.jpg');
INSERT INTO weapon VALUES ('The Inward Lamp', '2', 'Trials', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/The%20Inward%20Lamp.jpg');
INSERT INTO weapon VALUES ('Badger CCL', '1', 'Vanguard', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Badger%20CCL.jpg');
INSERT INTO weapon VALUES ('A.1F19X-Ryl', '1', 'Vanguard', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/A.1F19X-Ryl.jpg');
INSERT INTO weapon VALUES ('The Aries Nemesis X4', '1', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/The%20Aries%20Nemesis%20X4.jpg');
INSERT INTO weapon VALUES ('Gheleon''s Demise', '1', 'Iron Banner', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Gheleon''s%20Demise.jpg');
INSERT INTO weapon VALUES ('Colovance''s Duty', '2', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Colovance''s%20Duty.jpg');
INSERT INTO weapon VALUES ('Proxima Centauri II', '1', 'Vanguard', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Proxima%20Centauri%20II.jpg');
INSERT INTO weapon VALUES ('Last Extremity', '2', 'Crucible Qtr', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Last%20Extremity.jpg');
INSERT INTO weapon VALUES ('Fate of All Fools', '1', 'n/a', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/Fate%20of%20All%20Fools.jpg');
INSERT INTO weapon VALUES ('Doom of Chelchis', '2', 'King''s Fall', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Doom%20of%20Chelchis.jpg');
INSERT INTO weapon VALUES ('NA3D1 Salvation State', '1', 'HoW', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/NA3D1%20Salvation%20State.jpg');
INSERT INTO weapon VALUES ('Boolean Gemini', '2', 'Quest', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/Boolean%20Gemini.jpg');
INSERT INTO weapon VALUES ('Vision of Confluence', '1', 'VoG', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Vision%20of%20Confluence.jpg');
INSERT INTO weapon VALUES ('Zero Point LOTP', '1', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Zero%20Point%20LOTP.jpg');
INSERT INTO weapon VALUES ('Not Like the Others', '2', 'Vanguard Qtr', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Not%20Like%20the%20Others.jpg');
INSERT INTO weapon VALUES ('SUROS DIS-43', '2', 'Suros', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/SUROS%20DIS-43.jpg');
INSERT INTO weapon VALUES ('Fang of Ir Yut', '1', 'Crota HM', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Fang%20of%20Ir%20Yut.jpg');
INSERT INTO weapon VALUES ('The Saterienne Rapier', '1', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/The%20Saterienne%20Rapier.jpg');
INSERT INTO weapon VALUES ('Pallas Regime', '1', 'Q''s Wrath', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Pallas%20Regime.jpg');
INSERT INTO weapon VALUES ('Tuonela SR4', '2', 'Omolon', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Tuonela%20SR4.jpg');
INSERT INTO weapon VALUES ('Cryptic Dragon', '1', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Cryptic%20Dragon.jpg');
INSERT INTO weapon VALUES ('Another NitC', '1', 'Vanguard', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Another%20NitC.jpg');
INSERT INTO weapon VALUES ('Crusader I', '1', 'N Monarchy', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Crusader%20I.jpg');
INSERT INTO weapon VALUES ('Hung Jury SR4', '2', 'Dead Orbit', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Hung%20Jury%20SR4.jpg');
INSERT INTO weapon VALUES ('The Calling', '1', 'FWC', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/The%20Calling.jpg');
INSERT INTO weapon VALUES ('Hygiea Noblesse', '1', 'Q''s Chest', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Hygiea%20Noblesse.jpg');
INSERT INTO weapon VALUES ('Treads Upon Stars', '2', 'Strike', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Treads%20Upon%20Stars.jpg');
INSERT INTO weapon VALUES ('Tlaloc', '2', 'Gunsmith', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/Tlaloc.jpg');
INSERT INTO weapon VALUES ('Crypt Dweller SR1', '1', 'Dead Orbit', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Crypt%20Dweller%20SR1.jpg');
INSERT INTO weapon VALUES ('One/One Synesthete', '1', 'Vanguard', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/OneOneSynesthete.jpg');
INSERT INTO weapon VALUES ('High Road Soldier', '1', 'HoW', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/High%20Road%20Soldier.jpg');
INSERT INTO weapon VALUES ('MIDA Multi-Tool', '1', 'Various', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/MIDA%20Multi-Tool.jpg');
INSERT INTO weapon VALUES ('NL Shadow 701X', '2', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/NL%20Shadow%20701X.jpg');
INSERT INTO weapon VALUES ('Wolves'' Leash', '1', 'Varik', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Wolves''%20Leash.jpg');
INSERT INTO weapon VALUES ('Wolves'' Leash II', '1', 'Skolas', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Wolves''%20Leash%20II.jpg');
INSERT INTO weapon VALUES ('SUROS DIS-47', '2', 'Suros', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/SUROS%20DIS-47.jpg');
INSERT INTO weapon VALUES ('B-Line Trauma', '1', 'Crucible', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/B-Line%20Trauma.jpg');
INSERT INTO weapon VALUES ('Deadshot Luna SR1', '1', 'Dead Orbit', 'Scout Rifle', 'Primary', 'Legendary', '../../images/ScoutRifle/Deadshot%20Luna%20SR1.jpg');
INSERT INTO weapon VALUES ('Touch of Malice', '2', 'Quest', 'Scout Rifle', 'Primary', 'Exotic', '../../images/ScoutRifle/Touch%20of%20Malice.jpg');
INSERT INTO baseStat VALUES ('The Jade Rabbit', '94', '24', '61', '65');
INSERT INTO baseStat VALUES ('The Scholar', '77', '40', '61', '58');
INSERT INTO baseStat VALUES ('The Inward Lamp', '77', '40', '61', '58');
INSERT INTO baseStat VALUES ('Badger CCL', '77', '40', '61', '57');
INSERT INTO baseStat VALUES ('A.1F19X-Ryl', '77', '40', '61', '52');
INSERT INTO baseStat VALUES ('The Aries Nemesis X4', '83', '31', '61', '63');
INSERT INTO baseStat VALUES ('Gheleon''s Demise', '84', '29', '61', '52');
INSERT INTO baseStat VALUES ('Colovance''s Duty', '84', '29', '61', '52');
INSERT INTO baseStat VALUES ('Proxima Centauri II', '87', '24', '61', '59');
INSERT INTO baseStat VALUES ('Last Extremity', '74', '36', '61', '59');
INSERT INTO baseStat VALUES ('Fate of All Fools', '94', '24', '59', '65');
INSERT INTO baseStat VALUES ('Doom of Chelchis', '51', '37', '49', '41');
INSERT INTO baseStat VALUES ('NA3D1 Salvation State', '76', '66', '48', '65');
INSERT INTO baseStat VALUES ('Boolean Gemini', '61', '53', '48', '68');
INSERT INTO baseStat VALUES ('Vision of Confluence', '59', '57', '48', '65');
INSERT INTO baseStat VALUES ('Zero Point LOTP', '61', '53', '48', '70');
INSERT INTO baseStat VALUES ('Not Like the Others', '65', '46', '48', '59');
INSERT INTO baseStat VALUES ('SUROS DIS-43', '77', '48', '48', '45');
INSERT INTO baseStat VALUES ('Fang of Ir Yut', '75', '49', '48', '67');
INSERT INTO baseStat VALUES ('The Saterienne Rapier', '74', '52', '48', '74');
INSERT INTO baseStat VALUES ('Pallas Regime', '71', '52', '48', '69');
INSERT INTO baseStat VALUES ('Tuonela SR4', '62', '69', '48', '62');
INSERT INTO baseStat VALUES ('Cryptic Dragon', '64', '49', '48', '65');
INSERT INTO baseStat VALUES ('Another NitC', '63', '50', '48', '62');
INSERT INTO baseStat VALUES ('Crusader I', '75', '46', '48', '72');
INSERT INTO baseStat VALUES ('Hung Jury SR4', '72', '63', '48', '75');
INSERT INTO baseStat VALUES ('The Calling', '77', '44', '48', '64');
INSERT INTO baseStat VALUES ('Hygiea Noblesse', '66', '47', '48', '64');
INSERT INTO baseStat VALUES ('Treads Upon Stars', '66', '38', '48', '66');
INSERT INTO baseStat VALUES ('Tlaloc', '59', '40', '48', '52');
INSERT INTO baseStat VALUES ('Crypt Dweller SR1', '59', '44', '38', '80');
INSERT INTO baseStat VALUES ('One/One Synesthete', '53', '52', '38', '74');
INSERT INTO baseStat VALUES ('High Road Soldier', '58', '41', '38', '79');
INSERT INTO baseStat VALUES ('MIDA Multi-Tool', '58', '49', '37', '100');
INSERT INTO baseStat VALUES ('NL Shadow 701X', '55', '40', '37', '65');
INSERT INTO baseStat VALUES ('Wolves'' Leash', '52', '43', '35', '61');
INSERT INTO baseStat VALUES ('Wolves'' Leash II', '52', '43', '35', '61');
INSERT INTO baseStat VALUES ('SUROS DIS-47', '54', '50', '35', '55');
INSERT INTO baseStat VALUES ('B-Line Trauma', '51', '46', '35', '51');
INSERT INTO baseStat VALUES ('Deadshot Luna SR1', '60', '33', '35', '67');
INSERT INTO baseStat VALUES ('Touch of Malice', '64', '40', '35', '51');
INSERT INTO crucibleStat VALUES('The Jade Rabbit', '1.60', '0.80/0.40', '75');
INSERT INTO crucibleStat VALUES('The Scholar', '1.60', '0.80', '40');
INSERT INTO crucibleStat VALUES('The Inward Lamp', '1.60', '0.80', '40');
INSERT INTO crucibleStat VALUES('Badger CCL', '1.60', '0.80', '39');
INSERT INTO crucibleStat VALUES('A.1F19X-Ryl', '1.60', '0.80', '39');
INSERT INTO crucibleStat VALUES('The Aries Nemesis X4', '1.60', '0.80', '29');
INSERT INTO crucibleStat VALUES('Gheleon''s Demise', '1.60', '0.80', '26');
INSERT INTO crucibleStat VALUES('Colovance''s Duty', '1.60', '0.80', '26');
INSERT INTO crucibleStat VALUES('Proxima Centauri II', '1.60', '0.80', '21');
INSERT INTO crucibleStat VALUES('Last Extremity', '1.60', '0.80', '21');
INSERT INTO crucibleStat VALUES('Fate of All Fools', '1.60', '0.80/0.40', '75');
INSERT INTO crucibleStat VALUES('Doom of Chelchis', '1.33', '1.00', '65');
INSERT INTO crucibleStat VALUES('NA3D1 Salvation State', '1.33', '1.00', '50');
INSERT INTO crucibleStat VALUES('Boolean Gemini', '1.33', '1.00', '75');
INSERT INTO crucibleStat VALUES('Vision of Confluence', '1.33', '1.00', '69');
INSERT INTO crucibleStat VALUES('Zero Point LOTP', '1.33', '1.00', '65');
INSERT INTO crucibleStat VALUES('Not Like the Others', '1.33', '1.00', '69');
INSERT INTO crucibleStat VALUES('SUROS DIS-43', '1.33', '1.00', '61');
INSERT INTO crucibleStat VALUES('Fang of Ir Yut', '1.33', '1.00', '50');
INSERT INTO crucibleStat VALUES('The Saterienne Rapier', '1.33', '1.00', '44');
INSERT INTO crucibleStat VALUES('Pallas Regime', '1.33', '1.00', '49');
INSERT INTO crucibleStat VALUES('Tuonela SR4', '1.33', '1.00', '44');
INSERT INTO crucibleStat VALUES('Cryptic Dragon', '1.33', '1.00', '60');
INSERT INTO crucibleStat VALUES('Another NitC', '1.33', '1.00', '61');
INSERT INTO crucibleStat VALUES('Crusader I', '1.33', '1.00', '42');
INSERT INTO crucibleStat VALUES('Hung Jury SR4', '1.33', '1.00', '26');
INSERT INTO crucibleStat VALUES('The Calling', '1.33', '1.00', '39');
INSERT INTO crucibleStat VALUES('Hygiea Noblesse', '1.33', '1.00', '43');
INSERT INTO crucibleStat VALUES('Treads Upon Stars', '1.33', '1.00', '44');
INSERT INTO crucibleStat VALUES('Tlaloc', '1.33', '1.00', '55');
INSERT INTO crucibleStat VALUES('Crypt Dweller SR1', '1.50', '0.90', '69');
INSERT INTO crucibleStat VALUES('One/One Synesthete', '1.50', '0.90', '70');
INSERT INTO crucibleStat VALUES('High Road Soldier', '1.50', '0.90', '40');
INSERT INTO crucibleStat VALUES('MIDA Multi-Tool', '1.50', '0.90', '90');
INSERT INTO crucibleStat VALUES('NL Shadow 701X', '1.40', '0.93', '40');
INSERT INTO crucibleStat VALUES('Wolves'' Leash', '1.40', '0.93', '82');
INSERT INTO crucibleStat VALUES('Wolves'' Leash II', '1.40', '0.93', '82');
INSERT INTO crucibleStat VALUES('SUROS DIS-47', '1.40', '0.93', '74');
INSERT INTO crucibleStat VALUES('B-Line Trauma', '1.40', '0.93', '82');
INSERT INTO crucibleStat VALUES('Deadshot Luna SR1', '1.40', '0.93', '66');
INSERT INTO crucibleStat VALUES('Touch of Malice', '1.40/0.93', '0.93/0.70', '40');

INSERT INTO weapon VALUES ('Timur''s Lash', '1', 'Iron Banner', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Timur''s%20Lash.jpg');
INSERT INTO weapon VALUES ('The First Curse', '2', 'Quest', 'Hand Cannon', 'Primary', 'Exotic', '../../images/HandCannon/The%20First%20Curse.jpg');
INSERT INTO weapon VALUES ('Judith-D', '2', 'Haake', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Judith-D.jpg');
INSERT INTO weapon VALUES ('Ill Will', '1', 'HoW', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Ill%20Will.jpg');
INSERT INTO weapon VALUES ('Uffern HC4', '2', 'Omolon', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Uffern%20HC4.jpg');
INSERT INTO weapon VALUES ('The Vanity', '2', 'FWC', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Vanity.jpg');
INSERT INTO weapon VALUES ('Eyasluna', '2', 'Crucible', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Eyasluna.jpg');
INSERT INTO weapon VALUES ('Hawkmoon', '2', 'Various', 'Hand Cannon', 'Primary', 'Exotic', '../../images/HandCannon/Hawkmoon.jpg');
INSERT INTO weapon VALUES ('Finnala''s Peril', '2', 'Iron Banner', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Finnala''s%20Peril.jpg');
INSERT INTO weapon VALUES ('The Fulcrum', '1', 'FWC', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Fulcrum.jpg');
INSERT INTO weapon VALUES ('Ace of Spades', '2', 'Gunsmith', 'Hand Cannon', 'Primary', 'Exotic', '../../images/HandCannon/Ace%20of%20Spades.jpg');
INSERT INTO weapon VALUES ('Fatebringer', '1', 'VoG HM', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Fatebringer.jpg');
INSERT INTO weapon VALUES ('Imago Loop', '2', 'Strike', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Imago%20Loop.jpg');
INSERT INTO weapon VALUES ('Lord High Fixer', '1', 'Crucible', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Lord%20High%20Fixer.jpg');
INSERT INTO weapon VALUES ('Jewel of Osiris (Adept)', '1', 'Lighthouse', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Jewel%20of%20Osiris%20(Adept).jpg');
INSERT INTO weapon VALUES ('The Water Star', '2', 'Trials', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Water%20Star.jpg');
INSERT INTO weapon VALUES ('The Devil You Know', '1', 'Vanguard', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Devil%20You%20Know.jpg');
INSERT INTO weapon VALUES ('Merciful', '1', 'Q''s Wrath', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Merciful.jpg');
INSERT INTO weapon VALUES ('Six Dreg Pride', '1', 'Varik', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Six%20Dreg%20Pride.jpg');
INSERT INTO weapon VALUES ('Red Hand IX', '1', 'N Monarchy', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Red%20Hand%20IX.jpg');
INSERT INTO weapon VALUES ('TFWPKY 1969', '1', 'Crucible', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/TFWPKY%201969.jpg');
INSERT INTO weapon VALUES ('Venation III', '1', 'Dead Orbit', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Venation%20III.jpg');
INSERT INTO weapon VALUES ('Byronic Hero', '2', 'Crucible Qtr', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Byronic%20Hero.jpg');
INSERT INTO weapon VALUES ('Her Mercy', '1', 'Q''s Chest', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Her%20Mercy.jpg');
INSERT INTO weapon VALUES ('Gaheris-D', '2', 'Haake', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Gaheris-D.jpg');
INSERT INTO weapon VALUES ('The Chance', '1', 'FWC', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Chance.jpg');
INSERT INTO weapon VALUES ('Loner.Rebel', '1', 'HoW', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Loner.Rebel.jpg');
INSERT INTO weapon VALUES ('Kumakatok HC4', '2', 'Omolon', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Kumakatok%20HC4.jpg');
INSERT INTO weapon VALUES ('Down and Doubt 00-0', '2', 'Vanguard', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Down%20and%20Doubt%2000-0.jpg');
INSERT INTO weapon VALUES ('Zaouli''s Bane', '2', 'King''s Fall', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Zaouli''s%20Bane.jpg');
INSERT INTO weapon VALUES ('Up the Ante', '1', 'Crucible', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Up%20the%20Ante.jpg');
INSERT INTO weapon VALUES ('Word of Crota', '1', 'Crota HM', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Word%20of%20Crota.jpg');
INSERT INTO weapon VALUES ('Appellant III', '2', 'N Monarchy', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Appellant%20III.jpg');
INSERT INTO weapon VALUES ('Jewel of Osiris', '1', 'Trials', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/Jewel%20of%20Osiris.jpg');
INSERT INTO weapon VALUES ('The Revelator', '2', 'Crucible', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Revelator.jpg');
INSERT INTO weapon VALUES ('Thorn', '1', 'Quest', 'Hand Cannon', 'Primary', 'Exotic', '../../images/HandCannon/Thorn.jpg');
INSERT INTO weapon VALUES ('The Devil You Don''t', '1', 'Vanguard', 'Hand Cannon', 'Primary', 'Legendary', '../../images/HandCannon/The%20Devil%20You%20Don''t.jpg');
INSERT INTO weapon VALUES ('The Last Word', '1', 'Various', 'Hand Cannon', 'Primary', 'Exotic', '../../images/HandCannon/The%20Last%20Word.jpg');
INSERT INTO baseStat VALUES ('Timur''s Lash', '48', '15', '94', '29');
INSERT INTO baseStat VALUES ('The First Curse', '32', '12', '94', '22');
INSERT INTO baseStat VALUES ('Judith-D', '41', '17', '94', '26');
INSERT INTO baseStat VALUES ('Ill Will', '50', '12', '94', '9');
INSERT INTO baseStat VALUES ('Uffern HC4', '24', '70', '87', '43');
INSERT INTO baseStat VALUES ('The Vanity', '24', '70', '87', '40');
INSERT INTO baseStat VALUES ('Eyasluna', '38', '51', '81', '39');
INSERT INTO baseStat VALUES ('Hawkmoon', '38', '51', '81', '39');
INSERT INTO baseStat VALUES ('Finnala''s Peril', '37', '36', '81', '36');
INSERT INTO baseStat VALUES ('The Fulcrum', '39', '34', '81', '51');
INSERT INTO baseStat VALUES ('Ace of Spades', '40', '32', '81', '33');
INSERT INTO baseStat VALUES ('Fatebringer', '40', '32', '81', '33');
INSERT INTO baseStat VALUES ('Imago Loop', '40', '32', '81', '33');
INSERT INTO baseStat VALUES ('Lord High Fixer', '38', '36', '81', '37');
INSERT INTO baseStat VALUES ('Jewel of Osiris (Adept)', '40', '32', '81', '43');
INSERT INTO baseStat VALUES ('The Water Star', '40', '32', '81', '43');
INSERT INTO baseStat VALUES ('The Devil You Know', '25', '42', '81', '33');
INSERT INTO baseStat VALUES ('Merciful', '28', '37', '81', '30');
INSERT INTO baseStat VALUES ('Six Dreg Pride', '30', '50', '81', '43');
INSERT INTO baseStat VALUES ('Red Hand IX', '21', '50', '81', '33');
INSERT INTO baseStat VALUES ('TFWPKY 1969', '28', '37', '81', '30');
INSERT INTO baseStat VALUES ('Venation III', '24', '44', '81', '37');
INSERT INTO baseStat VALUES ('Byronic Hero', '25', '41', '81', '30');
INSERT INTO baseStat VALUES ('Her Mercy', '25', '32', '81', '26');
INSERT INTO baseStat VALUES ('Gaheris-D', '23', '56', '81', '41');
INSERT INTO baseStat VALUES ('The Chance', '25', '43', '81', '38');
INSERT INTO baseStat VALUES ('Loner.Rebel', '32', '41', '81', '29');
INSERT INTO baseStat VALUES ('Kumakatok HC4', '15', '60', '74', '48');
INSERT INTO baseStat VALUES ('Down and Doubt 00-0', '15', '60', '74', '48');
INSERT INTO baseStat VALUES ('Zaouli''s Bane', '15', '42', '71', '16');
INSERT INTO baseStat VALUES ('Up the Ante', '23', '34', '68', '56');
INSERT INTO baseStat VALUES ('Word of Crota', '25', '45', '68', '45');
INSERT INTO baseStat VALUES ('Appellant III', '25', '27', '68', '56');
INSERT INTO baseStat VALUES ('Jewel of Osiris', '17', '41', '68', '53');
INSERT INTO baseStat VALUES ('The Revelator', '17', '41', '68', '53');
INSERT INTO baseStat VALUES ('Thorn', '38', '31', '68', '43');
INSERT INTO baseStat VALUES ('The Devil You Don''t', '17', '41', '68', '53');
INSERT INTO baseStat VALUES ('The Last Word', '8', '22', '68', '56');
INSERT INTO crucibleStat VALUES('Timur''s Lash', '1.50', '1.00/0.50', '53');
INSERT INTO crucibleStat VALUES('The First Curse', '1.50', '1.00/0.50', '90');
INSERT INTO crucibleStat VALUES('Judith-D', '1.50', '1.00/0.50', '60');
INSERT INTO crucibleStat VALUES('Ill Will', '1.50', '1.00/0.50', '50');
INSERT INTO crucibleStat VALUES('Uffern HC4', '1.30', '0.86', '50');
INSERT INTO crucibleStat VALUES('The Vanity', '1.30', '0.86', '41');
INSERT INTO crucibleStat VALUES('Eyasluna', '1.30', '0.86', '50');
INSERT INTO crucibleStat VALUES('Hawkmoon', '1.30/0.86', '0.86/0.40', '50');
INSERT INTO crucibleStat VALUES('Finnala''s Peril', '1.30', '0.86', '71');
INSERT INTO crucibleStat VALUES('The Fulcrum', '1.30', '0.86', '75');
INSERT INTO crucibleStat VALUES('Ace of Spades', '1.30', '0.86', '61');
INSERT INTO crucibleStat VALUES('Fatebringer', '1.30', '0.86', '61');
INSERT INTO crucibleStat VALUES('Imago Loop', '1.30', '0.86', '61');
INSERT INTO crucibleStat VALUES('Lord High Fixer', '1.30', '0.86', '65');
INSERT INTO crucibleStat VALUES('Jewel of Osiris (Adept)', '1.30', '0.86', '75');
INSERT INTO crucibleStat VALUES('The Water Star', '1.30', '0.86', '75');
INSERT INTO crucibleStat VALUES('The Devil You Know', '1.30', '0.86', '81');
INSERT INTO crucibleStat VALUES('Merciful', '1.30', '0.86', '76');
INSERT INTO crucibleStat VALUES('Six Dreg Pride', '1.30', '0.86', '65');
INSERT INTO crucibleStat VALUES('Red Hand IX', '1.30', '0.86', '89');
INSERT INTO crucibleStat VALUES('TFWPKY 1969', '1.30', '0.86', '76');
INSERT INTO crucibleStat VALUES('Venation III', '1.30', '0.86', '83');
INSERT INTO crucibleStat VALUES('Byronic Hero', '1.30', '0.86', '71');
INSERT INTO crucibleStat VALUES('Her Mercy', '1.30', '0.86', '66');
INSERT INTO crucibleStat VALUES('Gaheris-D', '1.30', '0.86', '80');
INSERT INTO crucibleStat VALUES('The Chance', '1.30', '0.86', '82');
INSERT INTO crucibleStat VALUES('Loner.Rebel', '1.30', '0.86', '50');
INSERT INTO crucibleStat VALUES('Kumakatok HC4', '1.09', '0.73', '74');
INSERT INTO crucibleStat VALUES('Down and Doubt 00-0', '1.09', '0.73', '56');
INSERT INTO crucibleStat VALUES('Zaouli''s Bane', '1.30', '0.86', '90');
INSERT INTO crucibleStat VALUES('Up the Ante', '1.09', '0.73', '76');
INSERT INTO crucibleStat VALUES('Word of Crota', '1.09', '0.73', '74');
INSERT INTO crucibleStat VALUES('Appellant III', '1.09', '0.73', '76');
INSERT INTO crucibleStat VALUES('Jewel of Osiris', '1.30', '0.86', '90');
INSERT INTO crucibleStat VALUES('The Revelator', '1.09', '0.73', '90');
INSERT INTO crucibleStat VALUES('Thorn', '1.09', '0.73', '50');
INSERT INTO crucibleStat VALUES('The Devil You Don''t', '1.09', '0.73', '90');
INSERT INTO crucibleStat VALUES('The Last Word', '0.80', '0.53', '30');

INSERT INTO weapon VALUES ('Super Pox VLO', '1', 'Vanguard', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Super%20Pox%20VLO.jpg');
INSERT INTO weapon VALUES ('Spare Change.25', '2', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Spare%20Change.25.jpg');
INSERT INTO weapon VALUES ('The Messenger', '1', 'Trials', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/The%20Messenger.jpg');
INSERT INTO weapon VALUES ('Lyudmila-D', '2', 'Haake', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Lyudmila-D.jpg');
INSERT INTO weapon VALUES ('Three Little Words', '1', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Three%20Little%20Words.jpg');
INSERT INTO weapon VALUES ('Fair and Square', '1', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Fair%20and%20Square.jpg');
INSERT INTO weapon VALUES ('Strange Suspect', '1', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Strange%20Suspect.jpg');
INSERT INTO weapon VALUES ('KAU8 Constellation Scar', '1', 'HoW', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/KAU8%20Constellation%20Scar.jpg');
INSERT INTO weapon VALUES ('Nirwen''s Mercy', '2', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Nirwen''s%20Mercy.jpg');
INSERT INTO weapon VALUES ('SUROS PDX-41', '2', 'Suros', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/SUROS%20PDX-41.jpg');
INSERT INTO weapon VALUES ('The Villainy', '2', 'FWC', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/The%20Villainy.jpg');
INSERT INTO weapon VALUES ('Reflection Sum', '2', 'Trials', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Reflection%20Sum.jpg');
INSERT INTO weapon VALUES ('The Ninth Edict', '1', 'Q''s Wrath', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/The%20Ninth%20Edict.jpg');
INSERT INTO weapon VALUES ('Apple of Discord', '2', 'Vanguard Qtr', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Apple%20of%20Discord.jpg');
INSERT INTO weapon VALUES ('Hopscotch Pilgrim', '1', 'HoW', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Hopscotch%20Pilgrim.jpg');
INSERT INTO weapon VALUES ('Herja-D', '2', 'Haake', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Herja-D.jpg');
INSERT INTO weapon VALUES ('Bad Seed Down', '1', 'Vanguard', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Bad%20Seed%20Down.jpg');
INSERT INTO weapon VALUES ('55A-allFATE', '1', 'Vanguard', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/55A-allFATE.jpg');
INSERT INTO weapon VALUES ('Payment VI', '1', 'HoW', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Payment%20VI.jpg');
INSERT INTO weapon VALUES ('Red Death', '2', 'Various', 'Pulse Rifle', 'Primary', 'Exotic', '../../images/PulseRifle/Red%20Death.jpg');
INSERT INTO weapon VALUES ('Smite of Merain', '2', 'King''s Fall', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Smite%20of%20Merain.jpg');
INSERT INTO weapon VALUES ('SUROS PDX-45', '2', 'Suros', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/SUROS%20PDX-45.jpg');
INSERT INTO weapon VALUES ('Time On Target', '1', 'Vanguard', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Time%20On%20Target.jpg');
INSERT INTO weapon VALUES ('The Conduit', '1', 'FWC', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/The%20Conduit.jpg');
INSERT INTO weapon VALUES ('Aegis of the Kell', '1', 'Varik', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Aegis%20of%20the%20Kell.jpg');
INSERT INTO weapon VALUES ('Hawksaw', '2', 'Crucible Qtr', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Hawksaw.jpg');
INSERT INTO weapon VALUES ('Bad Juju', '2', 'Various', 'Pulse Rifle', 'Primary', 'Exotic', '../../images/PulseRifle/Bad%20Juju.jpg');
INSERT INTO weapon VALUES ('Praedyth''s Timepiece', '1', 'VoG', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Praedyth''s%20Timepiece.jpg');
INSERT INTO weapon VALUES ('Oversoul Edict', '1', 'Crota''s End', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Oversoul%20Edict.jpg');
INSERT INTO weapon VALUES ('Grasp of Malok', '2', 'Strike', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Grasp%20of%20Malok.jpg');
INSERT INTO weapon VALUES ('123 Syzygy', '1', 'Crucible', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/123%20Syzygy.jpg');
INSERT INTO weapon VALUES ('Skorri''s Revenge', '1', 'Iron Banner', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Skorri''s%20Revenge.jpg');
INSERT INTO weapon VALUES ('Coiled Hiss 1919', '1', 'Vanguard', 'Pulse Rifle', 'Primary', 'Legendary', '../../images/PulseRifle/Coiled%20Hiss%201919.jpg');
INSERT INTO baseStat VALUES ('Super Pox VLO', '53', '66', '30', '64');
INSERT INTO baseStat VALUES ('Spare Change.25', '63', '44', '30', '58');
INSERT INTO baseStat VALUES ('The Messenger', '59', '59', '30', '53');
INSERT INTO baseStat VALUES ('Lyudmila-D', '63', '54', '30', '51');
INSERT INTO baseStat VALUES ('Three Little Words', '61', '57', '30', '55');
INSERT INTO baseStat VALUES ('Fair and Square', '61', '57', '30', '52');
INSERT INTO baseStat VALUES ('Strange Suspect', '34', '71', '14', '65');
INSERT INTO baseStat VALUES ('KAU8 Constellation Scar', '54', '58', '14', '65');
INSERT INTO baseStat VALUES ('Nirwen''s Mercy', '34', '71', '14', '68');
INSERT INTO baseStat VALUES ('SUROS PDX-41', '48', '68', '14', '62');
INSERT INTO baseStat VALUES ('The Villainy', '39', '65', '14', '71');
INSERT INTO baseStat VALUES ('Reflection Sum', '43', '71', '14', '62');
INSERT INTO baseStat VALUES ('The Ninth Edict', '40', '64', '14', '71');
INSERT INTO baseStat VALUES ('Apple of Discord', '44', '59', '14', '68');
INSERT INTO baseStat VALUES ('Hopscotch Pilgrim', '45', '68', '14', '62');
INSERT INTO baseStat VALUES ('Herja-D', '33', '72', '14', '68');
INSERT INTO baseStat VALUES ('Bad Seed Down', '44', '59', '14', '67');
INSERT INTO baseStat VALUES ('55A-allFATE', '47', '65', '14', '62');
INSERT INTO baseStat VALUES ('Payment VI', '37', '60', '14', '66');
INSERT INTO baseStat VALUES ('Red Death', '33', '51', '14', '76');
INSERT INTO baseStat VALUES ('Smite of Merain', '44', '59', '14', '45');
INSERT INTO baseStat VALUES ('SUROS PDX-45', '22', '75', '7', '90');
INSERT INTO baseStat VALUES ('Time On Target', '35', '59', '7', '81');
INSERT INTO baseStat VALUES ('The Conduit', '35', '59', '7', '78');
INSERT INTO baseStat VALUES ('Aegis of the Kell', '28', '68', '7', '78');
INSERT INTO baseStat VALUES ('Hawksaw', '27', '72', '7', '80');
INSERT INTO baseStat VALUES ('Bad Juju', '33', '62', '7', '65');
INSERT INTO baseStat VALUES ('Praedyth''s Timepiece', '25', '64', '4', '54');
INSERT INTO baseStat VALUES ('Oversoul Edict', '33', '54', '4', '61');
INSERT INTO baseStat VALUES ('Grasp of Malok', '33', '54', '4', '61');
INSERT INTO baseStat VALUES ('123 Syzygy', '25', '64', '4', '48');
INSERT INTO baseStat VALUES ('Skorri''s Revenge', '30', '58', '4', '48');
INSERT INTO baseStat VALUES ('Coiled Hiss 1919', '33', '54', '4', '53');
INSERT INTO crucibleStat VALUES('Super Pox VLO', '1.26', '0.73', '37');
INSERT INTO crucibleStat VALUES('Spare Change.25', '1.26', '0.73', '50');
INSERT INTO crucibleStat VALUES('The Messenger', '1.26', '0.73', '35');
INSERT INTO crucibleStat VALUES('Lyudmila-D', '1.26', '0.73', '30');
INSERT INTO crucibleStat VALUES('Three Little Words', '1.26', '0.73', '23');
INSERT INTO crucibleStat VALUES('Fair and Square', '1.26', '0.73', '23');
INSERT INTO crucibleStat VALUES('Strange Suspect', '1.16', '0.67/0.97', '68');
INSERT INTO crucibleStat VALUES('KAU8 Constellation Scar', '1.16', '0.67/0.97', '60');
INSERT INTO crucibleStat VALUES('Nirwen''s Mercy', '1.16', '0.67/0.97', '65');
INSERT INTO crucibleStat VALUES('SUROS PDX-41', '1.16', '0.67/0.97', '55');
INSERT INTO crucibleStat VALUES('The Villainy', '1.16', '0.67/0.97', '60');
INSERT INTO crucibleStat VALUES('Reflection Sum', '1.16', '0.67/0.97', '54');
INSERT INTO crucibleStat VALUES('The Ninth Edict', '1.16', '0.67/0.97', '59');
INSERT INTO crucibleStat VALUES('Apple of Discord', '1.39', '0.76/1.13', '60');
INSERT INTO crucibleStat VALUES('Hopscotch Pilgrim', '1.16', '0.67/0.97', '50');
INSERT INTO crucibleStat VALUES('Herja-D', '1.39', '0.76/1.13', '50');
INSERT INTO crucibleStat VALUES('Bad Seed Down', '1.16', '0.67/0.97', '52');
INSERT INTO crucibleStat VALUES('55A-allFATE', '1.16', '0.67/0.97', '45');
INSERT INTO crucibleStat VALUES('Payment VI', '1.16', '0.67/0.97', '51');
INSERT INTO crucibleStat VALUES('Red Death', '1.03/1.16', '0.67', '50');
INSERT INTO crucibleStat VALUES('Smite of Merain', '1.16', '0.67/0.97', '21');
INSERT INTO crucibleStat VALUES('SUROS PDX-45', '1.30/1.76', '0.96', '70');
INSERT INTO crucibleStat VALUES('Time On Target', '1.30/1.76', '0.96', '67');
INSERT INTO crucibleStat VALUES('The Conduit', '1.30/1.76', '0.96', '67');
INSERT INTO crucibleStat VALUES('Aegis of the Kell', '1.30/1.76', '0.96', '65');
INSERT INTO crucibleStat VALUES('Hawksaw', '1.30/1.76', '0.96', '60');
INSERT INTO crucibleStat VALUES('Bad Juju', '1.30/1.76', '0.96', '60');
INSERT INTO crucibleStat VALUES('Praedyth''s Timepiece', '1.32', '0.80', '84');
INSERT INTO crucibleStat VALUES('Oversoul Edict', '1.32', '0.80', '82');
INSERT INTO crucibleStat VALUES('Grasp of Malok', '1.32', '0.80', '82');
INSERT INTO crucibleStat VALUES('123 Syzygy', '1.32', '0.80', '84');
INSERT INTO crucibleStat VALUES('Skorri''s Revenge', '1.32', '0.80', '75');
INSERT INTO crucibleStat VALUES('Coiled Hiss 1919', '1.32', '0.80', '70');

INSERT INTO weapon VALUES ('Matador 64', '1', 'HoW', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Matador%2064.jpg');
INSERT INTO weapon VALUES ('Binary Dawn', '2', 'Trials', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Binary%20Dawn.jpg');
INSERT INTO weapon VALUES ('Astral Horizon', '1', 'Trials', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Astral%20Horizon.jpg');
INSERT INTO weapon VALUES ('Conspiracy Theory-D', '2', 'Quest/Vanguard', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Conspiracy%20Theory-D.jpg');
INSERT INTO weapon VALUES ('Felwinter''s Lie', '1', 'Iron Banner', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Felwinter''s%20Lie.jpg');
INSERT INTO weapon VALUES ('Party Crasher +1', '2', 'Crucible', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Party%20Crasher%20+1.jpg');
INSERT INTO weapon VALUES ('Lord of Wolves', '1', 'POE', 'Shotgun', 'Special', 'Exotic', '../../images/Shotgun/Lord%20of%20Wolves.jpg');
INSERT INTO weapon VALUES ('Her Unspoken Will', '1', 'Q''s Wrath', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Her%20Unspoken%20Will.jpg');
INSERT INTO weapon VALUES ('Found Verdict', '1', 'VoG', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Found%20Verdict.jpg');
INSERT INTO weapon VALUES ('Judgment VI', '1', 'N Monarchy', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Judgment%20VI.jpg');
INSERT INTO weapon VALUES ('Deidris''s Retort', '2', 'Iron Banner', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Deidris''s%20Retort.jpg');
INSERT INTO weapon VALUES ('Two To The Morgue', '1', 'Crucible', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Two%20To%20The%20Morgue.jpg');
INSERT INTO weapon VALUES ('Silence of A''arn', '2', 'King''s Fall', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Silence%20of%20A''arn.jpg');
INSERT INTO weapon VALUES ('Her Courtesy', '1', 'Q''s Chest', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Her%20Courtesy.jpg');
INSERT INTO weapon VALUES ('Jingukogo-D', '2', 'Haake', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Jingukogo-D.jpg');
INSERT INTO weapon VALUES ('The Next Big Thing', '2', 'Crucible Qtr', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/The%20Next%20Big%20Thing.jpg');
INSERT INTO weapon VALUES ('Patch-A', '2', 'Dead Orbit', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Patch-A.jpg');
INSERT INTO weapon VALUES ('Strongbow-D', '2', 'Haake', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Strongbow-D.jpg');
INSERT INTO weapon VALUES ('Hide and Seek-42', '1', 'Dead Orbit', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Hide%20and%20Seek-42.jpg');
INSERT INTO weapon VALUES ('The Comedian', '1', 'Vanguard', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/The%20Comedian.jpg');
INSERT INTO weapon VALUES ('Rude Awakening DOA', '1', 'Dead Orbit', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Rude%20Awakening%20DOA.jpg');
INSERT INTO weapon VALUES ('The Crash', '1', 'FWC', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/The%20Crash.jpg');
INSERT INTO weapon VALUES ('Hard Luck Charm', '1', 'Crucible', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Hard%20Luck%20Charm.jpg');
INSERT INTO weapon VALUES ('Invective', '2', 'Various', 'Shotgun', 'Special', 'Exotic', '../../images/Shotgun/Invective.jpg');
INSERT INTO weapon VALUES ('The 4th Horseman', '2', 'Various', 'Shotgun', 'Special', 'Exotic', '../../images/Shotgun/The%204th%20Horseman.jpg');
INSERT INTO weapon VALUES ('The Chaperone', '2', 'Quest', 'Shotgun', 'Special', 'Exotic', '../../images/Shotgun/The%20Chaperone.jpg');
INSERT INTO weapon VALUES ('Dry Rot 32', '1', 'HoW', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Dry%20Rot%2032.jpg');
INSERT INTO weapon VALUES ('Immobius', '2', 'Quest', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Immobius.jpg');
INSERT INTO weapon VALUES ('Burden of Proof XI', '2', 'N Monarchy', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Burden%20of%20Proof%20XI.jpg');
INSERT INTO weapon VALUES ('Swordbreaker', '1', 'Crota''s End', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Swordbreaker.jpg');
INSERT INTO weapon VALUES ('Wolfborne Oath', '1', 'Varik', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Wolfborne%20Oath.jpg');
INSERT INTO weapon VALUES ('Invisible Hand M7', '1', 'Crucible', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Invisible%20Hand%20M7.jpg');
INSERT INTO weapon VALUES ('Secret Handshake', '1', 'Vanguard', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/Secret%20Handshake.jpg');
INSERT INTO weapon VALUES ('Universal Remote', '1', 'Quest', 'Shotgun', 'Special', 'Exotic', '../../images/Shotgun/Universal%20Remote.jpg');
INSERT INTO weapon VALUES ('In Times of Need', '2', 'Crucible', 'Shotgun', 'Special', 'Legendary', '../../images/Shotgun/In%20Times%20of%20Need.jpg');
INSERT INTO baseStat VALUES ('Matador 64', '25', '17', '67', '26');
INSERT INTO baseStat VALUES ('Binary Dawn', '25', '16', '67', '28');
INSERT INTO baseStat VALUES ('Astral Horizon', '25', '6', '67', '28');
INSERT INTO baseStat VALUES ('Conspiracy Theory-D', '16', '30', '67', '23');
INSERT INTO baseStat VALUES ('Felwinter''s Lie', '20', '6', '67', '23');
INSERT INTO baseStat VALUES ('Party Crasher +1', '23', '41', '64', '29');
INSERT INTO baseStat VALUES ('Lord of Wolves', '2', '12', '64', '22');
INSERT INTO baseStat VALUES ('Her Unspoken Will', '17', '47', '61', '33');
INSERT INTO baseStat VALUES ('Found Verdict', '20', '27', '61', '40');
INSERT INTO baseStat VALUES ('Judgment VI', '16', '24', '61', '46');
INSERT INTO baseStat VALUES ('Deidris''s Retort', '17', '36', '61', '23');
INSERT INTO baseStat VALUES ('Two To The Morgue', '14', '32', '61', '34');
INSERT INTO baseStat VALUES ('Silence of A''arn', '13', '45', '61', '20');
INSERT INTO baseStat VALUES ('Her Courtesy', '10', '31', '61', '29');
INSERT INTO baseStat VALUES ('Jingukogo-D', '12', '56', '52', '35');
INSERT INTO baseStat VALUES ('The Next Big Thing', '10', '60', '52', '35');
INSERT INTO baseStat VALUES ('Patch-A', '11', '57', '52', '35');
INSERT INTO baseStat VALUES ('Strongbow-D', '10', '56', '52', '33');
INSERT INTO baseStat VALUES ('Hide and Seek-42', '12', '23', '52', '42');
INSERT INTO baseStat VALUES ('The Comedian', '10', '29', '52', '39');
INSERT INTO baseStat VALUES ('Rude Awakening DOA', '7', '40', '52', '32');
INSERT INTO baseStat VALUES ('The Crash', '12', '25', '52', '31');
INSERT INTO baseStat VALUES ('Hard Luck Charm', '11', '26', '52', '32');
INSERT INTO baseStat VALUES ('Invective', '23', '51', '46', '36');
INSERT INTO baseStat VALUES ('The 4th Horseman', '5', '66', '42', '16');
INSERT INTO baseStat VALUES ('The Chaperone', '26', '100', '40', '29');
INSERT INTO baseStat VALUES ('Dry Rot 32', '11', '41', '40', '63');
INSERT INTO baseStat VALUES ('Immobius', '12', '32', '40', '50');
INSERT INTO baseStat VALUES ('Burden of Proof XI', '14', '27', '40', '50');
INSERT INTO baseStat VALUES ('Swordbreaker', '8', '34', '40', '53');
INSERT INTO baseStat VALUES ('Wolfborne Oath', '8', '32', '40', '44');
INSERT INTO baseStat VALUES ('Invisible Hand M7', '8', '21', '40', '50');
INSERT INTO baseStat VALUES ('Secret Handshake', '6', '27', '40', '48');
INSERT INTO baseStat VALUES ('Universal Remote', '14', '36', '40', '29');
INSERT INTO baseStat VALUES ('In Times of Need', '8', '41', '40', '29');
INSERT INTO crucibleStat VALUES('Matador 64', '0.66', '1.33', '50');
INSERT INTO crucibleStat VALUES('Binary Dawn', '0.66', '1.33', '24');
INSERT INTO crucibleStat VALUES('Astral Horizon', '0.66', '1.33', '24');
INSERT INTO crucibleStat VALUES('Conspiracy Theory-D', '0.75', '1.50', '26');
INSERT INTO crucibleStat VALUES('Felwinter''s Lie', '0.75', '1.50', '24');
INSERT INTO crucibleStat VALUES('Party Crasher +1', '0.66', '1.33', '70');
INSERT INTO crucibleStat VALUES('Lord of Wolves', 'None', 'None', '70');
INSERT INTO crucibleStat VALUES('Her Unspoken Will', '0.56', '1.13', '51');
INSERT INTO crucibleStat VALUES('Found Verdict', '0.56', '1.13', '36');
INSERT INTO crucibleStat VALUES('Judgment VI', '0.56', '1.13', '37');
INSERT INTO crucibleStat VALUES('Deidris''s Retort', '0.56', '1.13', '40');
INSERT INTO crucibleStat VALUES('Two To The Morgue', '0.56', '1.13', '46');
INSERT INTO crucibleStat VALUES('Silence of A''arn', '0.56', '1.13', '40');
INSERT INTO crucibleStat VALUES('Her Courtesy', '0.56', '1.13', '44');
INSERT INTO crucibleStat VALUES('Jingukogo-D', '0.46', '0.93', '55');
INSERT INTO crucibleStat VALUES('The Next Big Thing', '0.46', '0.93', '59');
INSERT INTO crucibleStat VALUES('Patch-A', '0.46', '0.93', '51');
INSERT INTO crucibleStat VALUES('Strongbow-D', '0.46', '0.93', '55');
INSERT INTO crucibleStat VALUES('Hide and Seek-42', '0.46', '0.93', '51');
INSERT INTO crucibleStat VALUES('The Comedian', '0.46', '0.93', '58');
INSERT INTO crucibleStat VALUES('Rude Awakening DOA', '0.46', '0.93', '69');
INSERT INTO crucibleStat VALUES('The Crash', '0.46', '0.93', '53');
INSERT INTO crucibleStat VALUES('Hard Luck Charm', '0.46', '0.93', '51');
INSERT INTO crucibleStat VALUES('Invective', '0.36', '0.36', '60');
INSERT INTO crucibleStat VALUES('The 4th Horseman', '0.16', '0.16', '80');
INSERT INTO crucibleStat VALUES('The Chaperone', 'None', 'None', '70');
INSERT INTO crucibleStat VALUES('Dry Rot 32', '0.33', '0.66', '50');
INSERT INTO crucibleStat VALUES('Immobius', '0.33', '0.66', '76');
INSERT INTO crucibleStat VALUES('Burden of Proof XI', '0.33', '0.66', '61');
INSERT INTO crucibleStat VALUES('Swordbreaker', '0.33', '0.66', '75');
INSERT INTO crucibleStat VALUES('Wolfborne Oath', '0.33', '0.66', '61');
INSERT INTO crucibleStat VALUES('Invisible Hand M7', '0.33', '0.66', '64');
INSERT INTO crucibleStat VALUES('Secret Handshake', '0.33', '0.66', '71');
INSERT INTO crucibleStat VALUES('Universal Remote', 'None', '1.13', '0');
INSERT INTO crucibleStat VALUES('In Times of Need', '0.33', '0.66', '0');

INSERT INTO weapon VALUES ('Vestian Dynasty', '1', 'Quest', 'Sidearm', 'Special', 'Legendary', '../../images/Sidearm/Vestian%20Dynasty.jpg');
INSERT INTO weapon VALUES ('Havoc Pigeon', '2', 'Crucible Qtr', 'Sidearm', 'Special', 'Legendary', '../../images/Sidearm/Havoc%20Pigeon.jpg');
INSERT INTO weapon VALUES ('Conviction II', '2', 'N Monarchy', 'Sidearm', 'Special', 'Legendary', '../../images/Sidearm/Conviction%20II.jpg');
INSERT INTO weapon VALUES ('JabberHakke-D', '2', 'Vanguard', 'Sidearm', 'Special', 'Legendary', '../../images/Sidearm/JabberHakke-D.jpg');
INSERT INTO weapon VALUES ('Ironwreath-D', '2', 'Iron Banner', 'Sidearm', 'Special', 'Legendary', '../../images/Sidearm/Ironwreath-D.jpg');
INSERT INTO weapon VALUES ('Dreg''s Promise', '1', 'POE', 'Sidearm', 'Special', 'Exotic', '../../images/Sidearm/Dreg''s%20Promise.jpg');
INSERT INTO baseStat VALUES ('Vestian Dynasty', '39', '90', '5', '96');
INSERT INTO baseStat VALUES ('Havoc Pigeon', '40', '81', '5', '85');
INSERT INTO baseStat VALUES ('Conviction II', '39', '78', '5', '86');
INSERT INTO baseStat VALUES ('JabberHakke-D', '38', '80', '5', '87');
INSERT INTO baseStat VALUES ('Ironwreath-D', '37', '82', '5', '88');
INSERT INTO baseStat VALUES ('Dreg''s Promise', '41', '79', '5', '86');
INSERT INTO crucibleStat VALUES('Vestian Dynasty', '0.80', '0.60', '90');
INSERT INTO crucibleStat VALUES('Havoc Pigeon', '0.80', '0.60', '85');
INSERT INTO crucibleStat VALUES('Conviction II', '0.80', '0.60', '89');
INSERT INTO crucibleStat VALUES('JabberHakke-D', '0.80', '0.60', '80');
INSERT INTO crucibleStat VALUES('Ironwreath-D', '0.80', '0.60', '71');
INSERT INTO crucibleStat VALUES('Dreg''s Promise', '0.51', '0.45', '70');

INSERT INTO weapon VALUES ('Black Spindle', '2', 'Mission', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/Black%20Spindle.jpg');
INSERT INTO weapon VALUES ('Efrideet''s Spear', '1', 'Iron Banner', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Efrideet''s%20Spear.jpg');
INSERT INTO weapon VALUES ('Black Hammer', '1', 'Crota''s End', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Black%20Hammer.jpg');
INSERT INTO weapon VALUES ('Eirene RR4', '2', 'Omolon', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Eirene%20RR4.jpg');
INSERT INTO weapon VALUES ('Stillpiercer', '2', 'Quest', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Stillpiercer.jpg');
INSERT INTO weapon VALUES ('Y-09 Longbow Synthesis', '1', 'Crucible', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Y-09%20Longbow%20Synthesis.jpg');
INSERT INTO weapon VALUES ('The Supremacy', '1', 'Q''s Wrath', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/The%20Supremacy.jpg');
INSERT INTO weapon VALUES ('1000-Yard Stare', '2', 'Vanguard', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/1000-Yard%20Stare.jpg');
INSERT INTO weapon VALUES ('No Land Beyond', '1', 'Various', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/No%20Land%20Beyond.jpg');
INSERT INTO weapon VALUES ('LDR 5001', '1', 'Vanguard', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/LDR%205001.jpg');
INSERT INTO weapon VALUES ('Shadow of Veils', '1', 'Varik', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Shadow%20of%20Veils.jpg');
INSERT INTO weapon VALUES ('Ice Breaker', '1', 'Various', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/Ice%20Breaker.jpg');
INSERT INTO weapon VALUES ('Her Benevolence', '1', 'Q''s Chest', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Her%20Benevolence.jpg');
INSERT INTO weapon VALUES ('Hereafter', '2', 'Various', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/Hereafter.jpg');
INSERT INTO weapon VALUES ('Patience and Time', '1', 'Various', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/Patience%20and%20Time.jpg');
INSERT INTO weapon VALUES ('Antinomy XVI', '2', 'N Monarchy', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Antinomy%20XVI.jpg');
INSERT INTO weapon VALUES ('Tamar-D', '2', 'Haake', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Tamar-D.jpg');
INSERT INTO weapon VALUES ('Broken Truth-LR1', '1', 'Dead Orbit', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Broken%20Truth-LR1.jpg');
INSERT INTO weapon VALUES ('Aoife Rua-D', '2', 'Haake', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Aoife%20Rua-D.jpg');
INSERT INTO weapon VALUES ('Low-Grade Humility', '1', 'HoW', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Low-Grade%20Humility.jpg');
INSERT INTO weapon VALUES ('Epitaph 2261', '1', 'Vanguard', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Epitaph%202261.jpg');
INSERT INTO weapon VALUES ('Defiance of Yasmin', '2', 'King''s Fall', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Defiance%20of%20Yasmin.jpg');
INSERT INTO weapon VALUES ('Queenbreakers'' Bow', '1', 'POE', 'Sniper Rifle', 'Special', 'Exotic', '../../images/SniperRifle/Queenbreakers''%20Bow.jpg');
INSERT INTO weapon VALUES ('Praedyth''s Revenge', '1', 'VoG', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Praedyth''s%20Revenge.jpg');
INSERT INTO weapon VALUES ('Extrasolar RR4', '2', 'Dead Orbit', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Extrasolar%20RR4.jpg');
INSERT INTO weapon VALUES ('Amplified Geo-D6', '1', 'Vanguard', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Amplified%20Geo-D6.jpg');
INSERT INTO weapon VALUES ('FINAL BOSS', '1', 'Crucible', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/FINAL%20BOSS.jpg');
INSERT INTO weapon VALUES ('Tao Hua Yuan', '2', 'Crucible Qtr', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Tao%20Hua%20Yuan.jpg');
INSERT INTO weapon VALUES ('Violator XII', '1', 'N Monarchy', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Violator%20XII.jpg');
INSERT INTO weapon VALUES ('The Chosen', '1', 'FWC', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/The%20Chosen.jpg');
INSERT INTO weapon VALUES ('Uzume RR4', '2', 'Omolon', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Uzume%20RR4.jpg');
INSERT INTO weapon VALUES ('Weyloran''s March', '2', 'Iron Banner', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Weyloran''s%20March.jpg');
INSERT INTO weapon VALUES ('Glass Promontory', '2', 'Trials', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Glass%20Promontory.jpg');
INSERT INTO weapon VALUES ('Eye of Sol', '1', 'Trials', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Eye%20of%20Sol.jpg');
INSERT INTO weapon VALUES ('Prudence II', '1', 'N Monarchy', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Prudence%20II.jpg');
INSERT INTO weapon VALUES ('Subtle Nudge DN7', '1', 'Crucible', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/Subtle%20Nudge%20DN7.jpg');
INSERT INTO weapon VALUES ('20/20 AMR7', '1', 'HoW', 'Sniper Rifle', 'Special', 'Legendary', '../../images/SniperRifle/2020%20AMR7.jpg');
INSERT INTO baseStat VALUES ('Black Spindle', '88', '35', '37', '55');
INSERT INTO baseStat VALUES ('Efrideet''s Spear', '88', '28', '37', '59');
INSERT INTO baseStat VALUES ('Black Hammer', '80', '35', '37', '55');
INSERT INTO baseStat VALUES ('Eirene RR4', '76', '44', '34', '57');
INSERT INTO baseStat VALUES ('Stillpiercer', '66', '57', '31', '65');
INSERT INTO baseStat VALUES ('Y-09 Longbow Synthesis', '66', '56', '31', '65');
INSERT INTO baseStat VALUES ('The Supremacy', '69', '52', '31', '64');
INSERT INTO baseStat VALUES ('1000-Yard Stare', '66', '57', '31', '62');
INSERT INTO baseStat VALUES ('No Land Beyond', '96', '24', '31', '62');
INSERT INTO baseStat VALUES ('LDR 5001', '68', '52', '31', '60');
INSERT INTO baseStat VALUES ('Shadow of Veils', '74', '41', '31', '59');
INSERT INTO baseStat VALUES ('Ice Breaker', '100', '15', '31', '62');
INSERT INTO baseStat VALUES ('Her Benevolence', '66', '46', '31', '60');
INSERT INTO baseStat VALUES ('Hereafter', '80', '49', '25', '51');
INSERT INTO baseStat VALUES ('Patience and Time', '75', '81', '22', '58');
INSERT INTO baseStat VALUES ('Antinomy XVI', '78', '36', '22', '73');
INSERT INTO baseStat VALUES ('Tamar-D', '78', '36', '22', '66');
INSERT INTO baseStat VALUES ('Broken Truth-LR1', '72', '46', '22', '66');
INSERT INTO baseStat VALUES ('Aoife Rua-D', '78', '34', '22', '68');
INSERT INTO baseStat VALUES ('Low-Grade Humility', '75', '41', '22', '65');
INSERT INTO baseStat VALUES ('Epitaph 2261', '74', '41', '22', '56');
INSERT INTO baseStat VALUES ('Defiance of Yasmin', '66', '57', '22', '41');
INSERT INTO baseStat VALUES ('Queenbreakers'' Bow', '60', '30', '22', '65');
INSERT INTO baseStat VALUES ('Praedyth''s Revenge', '68', '83', '16', '71');
INSERT INTO baseStat VALUES ('Extrasolar RR4', '78', '49', '16', '68');
INSERT INTO baseStat VALUES ('Amplified Geo-D6', '78', '49', '16', '65');
INSERT INTO baseStat VALUES ('FINAL BOSS', '80', '44', '16', '67');
INSERT INTO baseStat VALUES ('Tao Hua Yuan', '79', '47', '16', '62');
INSERT INTO baseStat VALUES ('Violator XII', '76', '52', '16', '61');
INSERT INTO baseStat VALUES ('The Chosen', '81', '43', '16', '63');
INSERT INTO baseStat VALUES ('Uzume RR4', '72', '58', '13', '75');
INSERT INTO baseStat VALUES ('Weyloran''s March', '60', '52', '13', '85');
INSERT INTO baseStat VALUES ('Glass Promontory', '70', '40', '13', '85');
INSERT INTO baseStat VALUES ('Eye of Sol', '70', '40', '13', '85');
INSERT INTO baseStat VALUES ('Prudence II', '63', '48', '13', '78');
INSERT INTO baseStat VALUES ('Subtle Nudge DN7', '60', '52', '10', '78');
INSERT INTO baseStat VALUES ('20/20 AMR7', '63', '32', '10', '79');
INSERT INTO crucibleStat VALUES('Black Spindle', '181', '452', '39');
INSERT INTO crucibleStat VALUES('Efrideet''s Spear', '181', '452', '10');
INSERT INTO crucibleStat VALUES('Black Hammer', '181', '452', '14');
INSERT INTO crucibleStat VALUES('Eirene RR4', '167', '417', '21');
INSERT INTO crucibleStat VALUES('Stillpiercer', '167', '417', '51');
INSERT INTO crucibleStat VALUES('Y-09 Longbow Synthesis', '167', '417', '53');
INSERT INTO crucibleStat VALUES('The Supremacy', '167', '417', '63');
INSERT INTO crucibleStat VALUES('1000-Yard Stare', '167', '417', '56');
INSERT INTO crucibleStat VALUES('No Land Beyond', '126/145', '233/267', '50');
INSERT INTO crucibleStat VALUES('LDR 5001', '167', '417', '49');
INSERT INTO crucibleStat VALUES('Shadow of Veils', '167', '417', '69');
INSERT INTO crucibleStat VALUES('Ice Breaker', '175', '438', '40');
INSERT INTO crucibleStat VALUES('Her Benevolence', '167', '417', '55');
INSERT INTO crucibleStat VALUES('Hereafter', '134', '334', '75');
INSERT INTO crucibleStat VALUES('Patience and Time', '134', '334', '55');
INSERT INTO crucibleStat VALUES('Antinomy XVI', '134', '334', '69');
INSERT INTO crucibleStat VALUES('Tamar-D', '134', '334', '65');
INSERT INTO crucibleStat VALUES('Broken Truth-LR1', '134', '334', '41');
INSERT INTO crucibleStat VALUES('Aoife Rua-D', '134', '334', '60');
INSERT INTO crucibleStat VALUES('Low-Grade Humility', '134', '334', '50');
INSERT INTO crucibleStat VALUES('Epitaph 2261', '134', '334', '36');
INSERT INTO crucibleStat VALUES('Defiance of Yasmin', '134', '334', '69');
INSERT INTO crucibleStat VALUES('Queenbreakers'' Bow', 'None', 'None', '80');
INSERT INTO crucibleStat VALUES('Praedyth''s Revenge', '107', '267', '20');
INSERT INTO crucibleStat VALUES('Extrasolar RR4', '107', '267', '36');
INSERT INTO crucibleStat VALUES('Amplified Geo-D6', '134', '334', '30');
INSERT INTO crucibleStat VALUES('FINAL BOSS', '107', '267', '24');
INSERT INTO crucibleStat VALUES('Tao Hua Yuan', '107', '267', '36');
INSERT INTO crucibleStat VALUES('Violator XII', '134', '334', '27');
INSERT INTO crucibleStat VALUES('The Chosen', '107', '267', '23');
INSERT INTO crucibleStat VALUES('Uzume RR4', '100', '250', '31');
INSERT INTO crucibleStat VALUES('Weyloran''s March', '100', '250', '79');
INSERT INTO crucibleStat VALUES('Glass Promontory', '100', '250', '61');
INSERT INTO crucibleStat VALUES('Eye of Sol', '100', '250', '61');
INSERT INTO crucibleStat VALUES('Prudence II', '100', '250', '74');
INSERT INTO crucibleStat VALUES('Subtle Nudge DN7', '87', '217', '49');
INSERT INTO crucibleStat VALUES('20/20 AMR7', '87', '217', '40');

INSERT INTO weapon VALUES ('Hitchhiker FR4', '2', 'Dead Orbit', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Hitchhiker%20FR4.jpg');
INSERT INTO weapon VALUES ('Servant of Aksor', '1', 'Varik', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Servant%20of%20Aksor.jpg');
INSERT INTO weapon VALUES ('77 Wizard', '1', 'Crucible', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/77%20Wizard.jpg');
INSERT INTO weapon VALUES ('Praetorian Foil', '1', 'VoG', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Praetorian%20Foil.jpg');
INSERT INTO weapon VALUES ('Darkblade''s Spite', '2', 'Strike', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Darkblade''s%20Spite.jpg');
INSERT INTO weapon VALUES ('The Vacancy', '2', 'FWC', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Vacancy.jpg');
INSERT INTO weapon VALUES ('The Exile''s Curse', '1', 'Trials', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Exile''s%20Curse.jpg');
INSERT INTO weapon VALUES ('The Trolley Problem', '1', 'Crucible', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Trolley%20Problem.jpg');
INSERT INTO weapon VALUES ('Thesan FR4', '2', 'Omolon', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Thesan%20FR4.jpg');
INSERT INTO weapon VALUES ('Midha''s Reckoning', '2', 'King''s Fall', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Midha''s%20Reckoning.jpg');
INSERT INTO weapon VALUES ('PLUG ONE.1', '1', 'Vanguard', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/PLUG%20ONE.1.jpg');
INSERT INTO weapon VALUES ('Purifier VII', '1', 'N Monarchy', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Purifier%20VII.jpg');
INSERT INTO weapon VALUES ('Snakebite Surgeon', '1', 'HoW', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Snakebite%20Surgeon.jpg');
INSERT INTO weapon VALUES ('GIVE/Take Equation', '1', 'Vanguard', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/GIVETake%20Equation.jpg');
INSERT INTO weapon VALUES ('Plan C', '1', 'Various', 'Fusion Rifle', 'Special', 'Exotic', '../../images/FusionRifle/Plan%20C.jpg');
INSERT INTO weapon VALUES ('Ashraven''s Flight', '2', 'Iron Banner', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Ashraven''s%20Flight.jpg');
INSERT INTO weapon VALUES ('Panta Rhei', '2', 'Crucible Qtr', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Panta%20Rhei.jpg');
INSERT INTO weapon VALUES ('Final Rest II', '1', 'Dead Orbit', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Final%20Rest%20II.jpg');
INSERT INTO weapon VALUES ('Techeun Force', '1', 'Q''s Wrath', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Techeun%20Force.jpg');
INSERT INTO weapon VALUES ('Susanoo', '2', 'Quest', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Susanoo.jpg');
INSERT INTO weapon VALUES ('LIGHT/Beware', '1', 'Vanguard', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/LIGHTBeware.jpg');
INSERT INTO weapon VALUES ('Pocket Infinity', '1', 'Quest', 'Fusion Rifle', 'Special', 'Exotic', '../../images/FusionRifle/Pocket%20Infinity.jpg');
INSERT INTO weapon VALUES ('The Vortex', '2', 'FWC', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Vortex.jpg');
INSERT INTO weapon VALUES ('Long Far Gone', '2', 'Quest/Vanguard', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Long%20Far%20Gone.jpg');
INSERT INTO weapon VALUES ('Murmur', '1', 'Eris', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Murmur.jpg');
INSERT INTO weapon VALUES ('Perun''s Fire', '1', 'Iron Banner', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Perun''s%20Fire.jpg');
INSERT INTO weapon VALUES ('Split Shifter Pro', '2', 'Crucible', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Split%20Shifter%20Pro.jpg');
INSERT INTO weapon VALUES ('The Frenzy', '1', 'FWC', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Frenzy.jpg');
INSERT INTO weapon VALUES ('Telesto', '2', 'Various', 'Fusion Rifle', 'Special', 'Exotic', '../../images/FusionRifle/Telesto.jpg');
INSERT INTO weapon VALUES ('Elevating Vision', '2', 'Trials', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Elevating%20Vision.jpg');
INSERT INTO weapon VALUES ('Exile''s Curse', '1', 'Trials', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Exile''s%20Curse.jpg');
INSERT INTO weapon VALUES ('The Calming', '1', 'FWC', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/The%20Calming.jpg');
INSERT INTO weapon VALUES ('Light of the Abyss', '1', 'Crota''s End', 'Fusion Rifle', 'Special', 'Legendary', '../../images/FusionRifle/Light%20of%20the%20Abyss.jpg');
INSERT INTO baseStat VALUES ('Hitchhiker FR4', '40', '36', '100', '52');
INSERT INTO baseStat VALUES ('Servant of Aksor', '56', '27', '97', '62');
INSERT INTO baseStat VALUES ('77 Wizard', '55', '28', '97', '62');
INSERT INTO baseStat VALUES ('Praetorian Foil', '52', '33', '97', '55');
INSERT INTO baseStat VALUES ('Darkblade''s Spite', '47', '33', '97', '55');
INSERT INTO baseStat VALUES ('The Vacancy', '40', '72', '95', '55');
INSERT INTO baseStat VALUES ('The Exile''s Curse', '50', '36', '95', '59');
INSERT INTO baseStat VALUES ('The Trolley Problem', '52', '32', '95', '61');
INSERT INTO baseStat VALUES ('Thesan FR4', '38', '81', '94', '68');
INSERT INTO baseStat VALUES ('Midha''s Reckoning', '41', '20', '94', '45');
INSERT INTO baseStat VALUES ('PLUG ONE.1', '42', '65', '91', '66');
INSERT INTO baseStat VALUES ('Purifier VII', '42', '65', '91', '55');
INSERT INTO baseStat VALUES ('Snakebite Surgeon', '40', '64', '90', '62');
INSERT INTO baseStat VALUES ('GIVE/Take Equation', '40', '65', '89', '61');
INSERT INTO baseStat VALUES ('Plan C', '52', '40', '87', '100');
INSERT INTO baseStat VALUES ('Ashraven''s Flight', '40', '46', '87', '71');
INSERT INTO baseStat VALUES ('Panta Rhei', '36', '54', '86', '59');
INSERT INTO baseStat VALUES ('Final Rest II', '44', '39', '84', '64');
INSERT INTO baseStat VALUES ('Techeun Force', '38', '42', '84', '56');
INSERT INTO baseStat VALUES ('Susanoo', '38', '50', '81', '71');
INSERT INTO baseStat VALUES ('LIGHT/Beware', '44', '40', '81', '59');
INSERT INTO baseStat VALUES ('Pocket Infinity', '28', '14', '78', '86');
INSERT INTO baseStat VALUES ('The Vortex', '38', '37', '76', '73');
INSERT INTO baseStat VALUES ('Long Far Gone', '31', '48', '76', '73');
INSERT INTO baseStat VALUES ('Murmur', '30', '37', '76', '58');
INSERT INTO baseStat VALUES ('Perun''s Fire', '38', '35', '75', '79');
INSERT INTO baseStat VALUES ('Split Shifter Pro', '35', '46', '74', '86');
INSERT INTO baseStat VALUES ('The Frenzy', '38', '36', '71', '82');
INSERT INTO baseStat VALUES ('Telesto', '35', '82', '71', '79');
INSERT INTO baseStat VALUES ('Elevating Vision', '28', '45', '71', '61');
INSERT INTO baseStat VALUES ('Exile''s Curse', '28', '45', '71', '61');
INSERT INTO baseStat VALUES ('The Calming', '35', '33', '71', '51');
INSERT INTO baseStat VALUES ('Light of the Abyss', '32', '41', '68', '61');
INSERT INTO crucibleStat VALUES('Hitchhiker FR4', 'NULL', 'NULL', '30');
INSERT INTO crucibleStat VALUES('Servant of Aksor', 'NULL', 'NULL', '39');
INSERT INTO crucibleStat VALUES('77 Wizard', 'NULL', 'NULL', '30');
INSERT INTO crucibleStat VALUES('Praetorian Foil', 'NULL', 'NULL', '36');
INSERT INTO crucibleStat VALUES('Darkblade''s Spite', 'NULL', 'NULL', '36');
INSERT INTO crucibleStat VALUES('The Vacancy', 'NULL', 'NULL', '60');
INSERT INTO crucibleStat VALUES('The Exile''s Curse', 'NULL', 'NULL', '39');
INSERT INTO crucibleStat VALUES('The Trolley Problem', 'NULL', 'NULL', '35');
INSERT INTO crucibleStat VALUES('Thesan FR4', 'NULL', 'NULL', '60');
INSERT INTO crucibleStat VALUES('Midha''s Reckoning', 'NULL', 'NULL', '39');
INSERT INTO crucibleStat VALUES('PLUG ONE.1', 'NULL', 'NULL', '56');
INSERT INTO crucibleStat VALUES('Purifier VII', 'NULL', 'NULL', '56');
INSERT INTO crucibleStat VALUES('Snakebite Surgeon', 'NULL', 'NULL', '60');
INSERT INTO crucibleStat VALUES('GIVE/Take Equation', 'NULL', 'NULL', '69');
INSERT INTO crucibleStat VALUES('Plan C', 'NULL', 'NULL', '40');
INSERT INTO crucibleStat VALUES('Ashraven''s Flight', 'NULL', 'NULL', '63');
INSERT INTO crucibleStat VALUES('Panta Rhei', 'NULL', 'NULL', '51');
INSERT INTO crucibleStat VALUES('Final Rest II', 'NULL', 'NULL', '68');
INSERT INTO crucibleStat VALUES('Techeun Force', 'NULL', 'NULL', '52');
INSERT INTO crucibleStat VALUES('Susanoo', 'NULL', 'NULL', '51');
INSERT INTO crucibleStat VALUES('LIGHT/Beware', 'NULL', 'NULL', '53');
INSERT INTO crucibleStat VALUES('Pocket Infinity', 'NULL', 'NULL', '30');
INSERT INTO crucibleStat VALUES('The Vortex', 'NULL', 'NULL', '79');
INSERT INTO crucibleStat VALUES('Long Far Gone', 'NULL', 'NULL', '70');
INSERT INTO crucibleStat VALUES('Murmur', 'NULL', 'NULL', '60');
INSERT INTO crucibleStat VALUES('Perun''s Fire', 'NULL', 'NULL', '63');
INSERT INTO crucibleStat VALUES('Split Shifter Pro', 'NULL', 'NULL', '50');
INSERT INTO crucibleStat VALUES('The Frenzy', 'NULL', 'NULL', '64');
INSERT INTO crucibleStat VALUES('Telesto', 'NULL', 'NULL', '40');
INSERT INTO crucibleStat VALUES('Elevating Vision', 'NULL', 'NULL', '84');
INSERT INTO crucibleStat VALUES('Exile''s Curse', 'NULL', 'NULL', '84');
INSERT INTO crucibleStat VALUES('The Calming', 'NULL', 'NULL', '70');
INSERT INTO crucibleStat VALUES('Light of the Abyss', 'NULL', 'NULL', '68');

INSERT INTO weapon VALUES ('Wolves'' Curse', '1', 'Q''s Wrath', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Wolves''%20Curse.jpg');
INSERT INTO weapon VALUES ('Jolder''s Hammer', '1', 'Iron Banner', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Jolder''s%20Hammer.jpg');
INSERT INTO weapon VALUES ('Against All Odds', '1', 'Crucible', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Against%20All%20Odds.jpg');
INSERT INTO weapon VALUES ('Wolves'' Bane', '1', 'Q''s Chest', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Wolves''%20Bane.jpg');
INSERT INTO weapon VALUES ('Thunderlord', '2', 'Various', 'Machine Gun', 'Heavy', 'Exotic', '../../images/MachineGun/Thunderlord.jpg');
INSERT INTO weapon VALUES ('Ruin Wake', '2', 'Crucible Qtr', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Ruin%20Wake.jpg');
INSERT INTO weapon VALUES ('BTRD-345', '1', 'HoW', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/BTRD-345.jpg');
INSERT INTO weapon VALUES ('Bretomart''s Stand', '2', 'Iron Banner', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Bretomart''s%20Stand.jpg');
INSERT INTO weapon VALUES ('The Unseeing Eye', '2', 'Trials', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/The%20Unseeing%20Eye.jpg');
INSERT INTO weapon VALUES ('Objection IV', '2', 'N Monarchy', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Objection%20IV.jpg');
INSERT INTO weapon VALUES ('The Infinite Theorem', '1', 'Trials', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/The%20Infinite%20Theorem.jpg');
INSERT INTO weapon VALUES ('Deviant Gravity-A', '1', 'Dead Orbit', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Deviant%20Gravity-A.jpg');
INSERT INTO weapon VALUES ('THE SWARM', '1', 'Vanguard', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/THE%20SWARM.jpg');
INSERT INTO weapon VALUES ('Pacifier X', '1', 'N Monarchy', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Pacifier%20X.jpg');
INSERT INTO weapon VALUES ('Qullim''s Terminus', '2', 'King''s Fall', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Qullim''s%20Terminus.jpg');
INSERT INTO weapon VALUES ('Unending Deluge III', '1', 'HoW', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Unending%20Deluge%20III.jpg');
INSERT INTO weapon VALUES ('Baron''s Ambition', '2', 'Strike', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Baron''s%20Ambition.jpg');
INSERT INTO weapon VALUES ('Chain of Orbiks-Fel', '1', 'Varik', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Chain%20of%20Orbiks-Fel.jpg');
INSERT INTO weapon VALUES ('Zombie Apocalypse WF47', '1', 'Crucible', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Zombie%20Apocalypse%20WF47.jpg');
INSERT INTO weapon VALUES ('Arma Engine DOA', '1', 'Dead Orbit', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Arma%20Engine%20DOA.jpg');
INSERT INTO weapon VALUES ('MG18A Harm''s Way', '1', 'Vanguard', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/MG18A%20Harm''s%20Way.jpg');
INSERT INTO weapon VALUES ('The Variable', '2', 'FWC', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/The%20Variable.jpg');
INSERT INTO weapon VALUES ('Super Good Advice', '1', 'Quest', 'Machine Gun', 'Heavy', 'Exotic', '../../images/MachineGun/Super%20Good%20Advice.jpg');
INSERT INTO weapon VALUES ('Corrective Measure', '1', 'VoG', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Corrective%20Measure.jpg');
INSERT INTO weapon VALUES ('Song of Ir Yut', '1', 'Crota''s End', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Song%20of%20Ir%20Yut.jpg');
INSERT INTO weapon VALUES ('Diluvian 10/4x', '2', 'Vanguard', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Diluvian%20104x.jpg');
INSERT INTO weapon VALUES ('Prestige IV', '1', 'N Monarchy', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Prestige%20IV.jpg');
INSERT INTO weapon VALUES ('Sawtooth Oscillator', '1', 'Vanguard', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Sawtooth%20Oscillator.jpg');
INSERT INTO weapon VALUES ('The Culling', '1', 'FWC', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/The%20Culling.jpg');
INSERT INTO weapon VALUES ('Backscratcher 9.0', '1', 'HoW', 'Machine Gun', 'Heavy', 'Legendary', '../../images/MachineGun/Backscratcher%209.0.jpg');
INSERT INTO baseStat VALUES ('Wolves'' Curse', '36', '25', '61', '35');
INSERT INTO baseStat VALUES ('Jolder''s Hammer', '28', '20', '61', '30');
INSERT INTO baseStat VALUES ('Against All Odds', '34', '8', '61', '30');
INSERT INTO baseStat VALUES ('Wolves'' Bane', '26', '12', '61', '31');
INSERT INTO baseStat VALUES ('Thunderlord', '32', '51', '53', '46');
INSERT INTO baseStat VALUES ('Ruin Wake', '15', '60', '53', '39');
INSERT INTO baseStat VALUES ('BTRD-345', '32', '80', '53', '22');
INSERT INTO baseStat VALUES ('Bretomart''s Stand', '15', '60', '53', '26');
INSERT INTO baseStat VALUES ('The Unseeing Eye', '15', '60', '53', '29');
INSERT INTO baseStat VALUES ('Objection IV', '20', '51', '53', '33');
INSERT INTO baseStat VALUES ('The Infinite Theorem', '12', '60', '53', '29');
INSERT INTO baseStat VALUES ('Deviant Gravity-A', '12', '46', '53', '33');
INSERT INTO baseStat VALUES ('THE SWARM', '10', '50', '53', '26');
INSERT INTO baseStat VALUES ('Pacifier X', '12', '46', '53', '27');
INSERT INTO baseStat VALUES ('Qullim''s Terminus', '15', '60', '53', '16');
INSERT INTO baseStat VALUES ('Unending Deluge III', '27', '51', '37', '36');
INSERT INTO baseStat VALUES ('Baron''s Ambition', '26', '50', '37', '33');
INSERT INTO baseStat VALUES ('Chain of Orbiks-Fel', '23', '50', '37', '33');
INSERT INTO baseStat VALUES ('Zombie Apocalypse WF47', '22', '36', '37', '34');
INSERT INTO baseStat VALUES ('Arma Engine DOA', '14', '37', '29', '31');
INSERT INTO baseStat VALUES ('MG18A Harm''s Way', '16', '34', '29', '33');
INSERT INTO baseStat VALUES ('The Variable', '18', '50', '29', '30');
INSERT INTO baseStat VALUES ('Super Good Advice', '14', '36', '29', '22');
INSERT INTO baseStat VALUES ('Corrective Measure', '18', '50', '27', '39');
INSERT INTO baseStat VALUES ('Song of Ir Yut', '8', '30', '25', '48');
INSERT INTO baseStat VALUES ('Diluvian 10/4x', '10', '30', '25', '37');
INSERT INTO baseStat VALUES ('Prestige IV', '10', '12', '25', '37');
INSERT INTO baseStat VALUES ('Sawtooth Oscillator', '6', '19', '25', '37');
INSERT INTO baseStat VALUES ('The Culling', '12', '7', '25', '32');
INSERT INTO baseStat VALUES ('Backscratcher 9.0', '15', '12', '25', '22');
INSERT INTO crucibleStat VALUES('Wolves'' Curse', '0.60', '0.40', '33');
INSERT INTO crucibleStat VALUES('Jolder''s Hammer', '0.60', '0.40', '28');
INSERT INTO crucibleStat VALUES('Against All Odds', '0.60', '0.40', '26');
INSERT INTO crucibleStat VALUES('Wolves'' Bane', '0.60', '0.40', '29');
INSERT INTO crucibleStat VALUES('Thunderlord', '0.50', '0.50', '90');
INSERT INTO crucibleStat VALUES('Ruin Wake', '0.50', '0.50', '67');
INSERT INTO crucibleStat VALUES('BTRD-345', '0.66', '0.50', '50');
INSERT INTO crucibleStat VALUES('Bretomart''s Stand', '0.50', '0.50', '74');
INSERT INTO crucibleStat VALUES('The Unseeing Eye', '0.50', '0.50', '70');
INSERT INTO crucibleStat VALUES('Objection IV', '0.50', '0.50', '65');
INSERT INTO crucibleStat VALUES('The Infinite Theorem', '0.50', '0.50', '70');
INSERT INTO crucibleStat VALUES('Deviant Gravity-A', '0.50', '0.50', '70');
INSERT INTO crucibleStat VALUES('THE SWARM', '0.50', '0.50', '74');
INSERT INTO crucibleStat VALUES('Pacifier X', '0.66', '0.50', '72');
INSERT INTO crucibleStat VALUES('Qullim''s Terminus', '0.50', '0.50', '65');
INSERT INTO crucibleStat VALUES('Unending Deluge III', '0.70', '0.50', '40');
INSERT INTO crucibleStat VALUES('Baron''s Ambition', '0.53', '0.40', '52');
INSERT INTO crucibleStat VALUES('Chain of Orbiks-Fel', '0.66', '0.53', '52');
INSERT INTO crucibleStat VALUES('Zombie Apocalypse WF47', '0.66', '0.53', '50');
INSERT INTO crucibleStat VALUES('Arma Engine DOA', '0.70', '0.50', '69');
INSERT INTO crucibleStat VALUES('MG18A Harm''s Way', '0.60', '0.50', '63');
INSERT INTO crucibleStat VALUES('The Variable', '0.60', '0.50', '51');
INSERT INTO crucibleStat VALUES('Super Good Advice', '0.70', '0.50', '30');
INSERT INTO crucibleStat VALUES('Corrective Measure', '0.60', '0.50', '69');
INSERT INTO crucibleStat VALUES('Song of Ir Yut', '0.60', '0.46', '80');
INSERT INTO crucibleStat VALUES('Diluvian 10/4x', '0.60', '0.46', '70');
INSERT INTO crucibleStat VALUES('Prestige IV', '0.60', '0.46', '75');
INSERT INTO crucibleStat VALUES('Sawtooth Oscillator', '0.60', '0.46', '71');
INSERT INTO crucibleStat VALUES('The Culling', '0.60', '0.46', '70');
INSERT INTO crucibleStat VALUES('Backscratcher 9.0', '0.60', '0.46', '30');

INSERT INTO weapon VALUES ('Truth', '2', 'Various', 'Rocket Launcher', 'Heavy', 'Exotic', '../../images/RocketLauncher/Truth.jpg');
INSERT INTO weapon VALUES ('The Dreammaker', '1', 'Q''s Wrath', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Dreammaker.jpg');
INSERT INTO weapon VALUES ('The Ash Factory', '2', 'Crucible', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Ash%20Factory.jpg');
INSERT INTO weapon VALUES ('Radegast''s Fury', '1', 'Iron Banner', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Radegast''s%20Fury.jpg');
INSERT INTO weapon VALUES ('The Hothead', '2', 'Quest', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Hothead.jpg');
INSERT INTO weapon VALUES ('The Last Rebellion', '1', 'Varik', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Last%20Rebellion.jpg');
INSERT INTO weapon VALUES ('Hezen Vengeance', '1', 'VoG', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Hezen%20Vengeance.jpg');
INSERT INTO weapon VALUES ('Dragon''s Breath', '1', 'Various', 'Rocket Launcher', 'Heavy', 'Exotic', '../../images/RocketLauncher/Dragon''s%20Breath.jpg');
INSERT INTO weapon VALUES ('Choleric Dragon SRT-49', '2', 'Quest/Vanguard', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Choleric%20Dragon%20SRT-49.jpg');
INSERT INTO weapon VALUES ('Elulim''s Frenzy', '2', 'King''s Fall', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Elulim''s%20Frenzy.jpg');
INSERT INTO weapon VALUES ('Admonisher III', '1', 'N Monarchy', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Admonisher%20III.jpg');
INSERT INTO weapon VALUES ('Valedictorian 9-44', '1', 'Crucible', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Valedictorian%209-44.jpg');
INSERT INTO weapon VALUES ('Steel Oracle Z-11', '1', 'Crucible', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Steel%20Oracle%20Z-11.jpg');
INSERT INTO weapon VALUES ('Wastelander V2V', '1', 'HoW', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Wastelander%20V2V.jpg');
INSERT INTO weapon VALUES ('The Tamarind', '2', 'Trials', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Tamarind.jpg');
INSERT INTO weapon VALUES ('Tomorrow''s Answer', '1', 'Trials', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Tomorrow''s%20Answer.jpg');
INSERT INTO weapon VALUES ('Ceres Lost BMJ-46', '2', 'Dead Orbit', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Ceres%20Lost%20BMJ-46.jpg');
INSERT INTO weapon VALUES ('SUROS JLB-47', '2', 'Suros', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/SUROS%20JLB-47.jpg');
INSERT INTO weapon VALUES ('The Vertigo', '2', 'FWC', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Vertigo.jpg');
INSERT INTO weapon VALUES ('SUROS JLB-42', '2', 'Suros', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/SUROS%20JLB-42.jpg');
INSERT INTO weapon VALUES ('Unfriendly Giant', '1', 'Vanguard', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Unfriendly%20Giant.jpg');
INSERT INTO weapon VALUES ('Gjallarhorn', '1', 'Various', 'Rocket Launcher', 'Heavy', 'Exotic', '../../images/RocketLauncher/Gjallarhorn.jpg');
INSERT INTO weapon VALUES ('Hunger of Crota', '1', 'Crota''s End', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Hunger%20of%20Crota.jpg');
INSERT INTO weapon VALUES ('Tormod''s Bellows', '2', 'Iron Banner', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Tormod''s%20Bellows.jpg');
INSERT INTO weapon VALUES ('The Cure', '1', 'FWC', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Cure.jpg');
INSERT INTO weapon VALUES ('One Way Ticket 000', '1', 'TDB', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/One%20Way%20Ticket%20000.jpg');
INSERT INTO weapon VALUES ('The Fear', '1', 'FWC', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Fear.jpg');
INSERT INTO weapon VALUES ('Exodus Plan RS/1', '1', 'Dead Orbit', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Exodus%20Plan%20RS1.jpg');
INSERT INTO weapon VALUES ('The Smolder', '2', 'Crucible Qtr', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/The%20Smolder.jpg');
INSERT INTO weapon VALUES ('Pax Totalus EPR8', '1', 'Crucible', 'Rocket Launcher', 'Heavy', 'Legendary', '../../images/RocketLauncher/Pax%20Totalus%20EPR8.jpg');
INSERT INTO rocketLauncherStat VALUES ('Truth', '48', '96', '83', '2', '79', '70');
INSERT INTO rocketLauncherStat VALUES ('The Dreammaker', '62', '96', '46', '1', '64', '36');
INSERT INTO rocketLauncherStat VALUES ('The Ash Factory', '63', '96', '45', '1', '52', '39');
INSERT INTO rocketLauncherStat VALUES ('Radegast''s Fury', '70', '96', '35', '1', '62', '23');
INSERT INTO rocketLauncherStat VALUES ('The Hothead', '39', '96', '66', '2', '52', '25');
INSERT INTO rocketLauncherStat VALUES ('The Last Rebellion', '39', '96', '66', '2', '54', '20');
INSERT INTO rocketLauncherStat VALUES ('Hezen Vengeance', '71', '96', '24', '2', '62', '31');
INSERT INTO rocketLauncherStat VALUES ('Dragon''s Breath', '33', '96', '66', '2', '52', '25');
INSERT INTO rocketLauncherStat VALUES ('Choleric Dragon SRT-49', '36', '96', '66', '2', '51', '11');
INSERT INTO rocketLauncherStat VALUES ('Elulim''s Frenzy', '41', '96', '58', '2', '38', '29');
INSERT INTO rocketLauncherStat VALUES ('Admonisher III', '30', '96', '53', '1', '57', '20');
INSERT INTO rocketLauncherStat VALUES ('Valedictorian 9-44', '30', '96', '56', '2', '48', '23');
INSERT INTO rocketLauncherStat VALUES ('Steel Oracle Z-11', '35', '96', '46', '2', '49', '11');
INSERT INTO rocketLauncherStat VALUES ('Wastelander V2V', '54', '84', '58', '2', '72', '60');
INSERT INTO rocketLauncherStat VALUES ('The Tamarind', '50', '84', '54', '2', '71', '66');
INSERT INTO rocketLauncherStat VALUES ('Tomorrow''s Answer', '50', '84', '46', '1', '71', '66');
INSERT INTO rocketLauncherStat VALUES ('Ceres Lost BMJ-46', '49', '84', '57', '2', '59', '69');
INSERT INTO rocketLauncherStat VALUES ('SUROS JLB-47', '74', '84', '44', '2', '51', '60');
INSERT INTO rocketLauncherStat VALUES ('The Vertigo', '74', '84', '45', '2', '51', '51');
INSERT INTO rocketLauncherStat VALUES ('SUROS JLB-42', '67', '84', '52', '2', '45', '51');
INSERT INTO rocketLauncherStat VALUES ('Unfriendly Giant', '47', '84', '37', '1', '64', '61');
INSERT INTO rocketLauncherStat VALUES ('Gjallarhorn', '90', '76', '53', '2', '68', '50');
INSERT INTO rocketLauncherStat VALUES ('Hunger of Crota', '78', '68', '62', '2', '71', '50');
INSERT INTO rocketLauncherStat VALUES ('Tormod''s Bellows', '83', '68', '51', '2', '65', '36');
INSERT INTO rocketLauncherStat VALUES ('The Cure', '71', '68', '46', '1', '75', '46');
INSERT INTO rocketLauncherStat VALUES ('One Way Ticket 000', '74', '68', '42', '1', '73', '42');
INSERT INTO rocketLauncherStat VALUES ('The Fear', '77', '68', '37', '2', '63', '54');
INSERT INTO rocketLauncherStat VALUES ('Exodus Plan RS/1', '77', '68', '38', '2', '62', '37');
INSERT INTO rocketLauncherStat VALUES ('The Smolder', '43', '60', '52', '2', '73', '79');
INSERT INTO rocketLauncherStat VALUES ('Pax Totalus EPR8', '47', '60', '21', '2', '83', '75');

INSERT INTO perk (perkName, description, perkType) VALUES
('Longview SLR10', 'Low-zoom scope. Improved range and handling.', 'Optic'),
('Aggressive Ballistics', 'More predictable recoil. Enhanced impact. Shorter range and more recoil.', 'Barrel'),
('Full Auto', 'This weapon can be fired in full auto mode.', 'Ability.');

INSERT INTO perkOnWeapons (perkName, weaponTypeName) VALUES
('Longview SLR10', 'Sniper Rifle'),
('Aggressive Ballistics', 'Shotgun'),
('Aggressive Ballistics', 'Machine Gun'),
('Full Auto', 'Pulse Rifle'),
('Full Auto', 'Scout Rifle'),
('Full Auto', 'Shotgun');

INSERT INTO optic (opticName, zoomRate, reloadMod, stabilityMod, handlingMod, aimAssistMod) VALUES
('Longview SLR10', NULL, NULL, NULL, NULL, 40);

INSERT INTO barrel VALUES
('Aggressive Ballistics', 5, -10, -10, -15); 






