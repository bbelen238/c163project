<?php
/**
 * Created by PhpStorm.
 * User: brenbln
 * Date: 4/27/16
 * Time: 5:42 PM
 */

include("../html/main.html");
include("../php/destiny_shared.php");

$weaponName = str_replace("+", " ", $_GET["name"]);
$weaponYear = $_GET["year"];
echo $weaponName . $weaponYear;

$sql = "SELECT weapon.imgPath, baseStat.weaponName, weapon.weaponTypeName, weapon.weaponSlotName, weapon.weaponRarity, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
        FROM baseStat 
        INNER JOIN weapon 
        ON baseStat.weaponName = weapon.weaponName
        INNER JOIN crucibleStat
        ON baseStat.weaponName = crucibleStat.weaponName
        WHERE weapon.weaponName = '" . $weaponName . "'" . ' AND weapon.weaponYear = ' . $weaponYear;

echo $sql;
$query = $db->prepare($sql);
$query->execute();
$coln = null;
$total = $query->columnCount();
for ($counter = 0; $counter < $total; $counter++) {
    $meta = $query->getColumnMeta($counter);
    $coln[$counter] = $meta['name'];
}

$rows = $query->fetchAll();
echo $rows[0]["weaponName"];


?>
<body>
<div class="container-fluid">
    <form role="form" method="post" >
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Edit a weapon</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Icon</th>
                    <th>Weapon Name</th>
                    <th>Weapon Type</th>
                    <th>Weapon Slot</th>
                    <th>Weapon Rarity</th>
                    <th>Year</th>
                    <th>Source</th>
                    <th>Weapon Range</th>
                    <th>Stability</th>
                    <th>Impact</th>
                    <th>Aim Assist</th>
                    <th>Reload Speed</th>
                    <th>BodyTTK</th>
                    <th>CritTTK</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($rows as $row) {
                    print "<tr>\n";
                    for ($counter = 0; $counter < $total; $counter++) {
                        if ($coln[$counter] == "weaponTypeName") {
                            ?>
                            <td>
                                <select class="form-control" type="text" name="weaponTypeName" >
                                    <option>Auto Rifle</option>
                                    <option>Scout Rifle</option>
                                    <option>Pulse Rifle</option>
                                    <option>Hand Cannon</option>
                                    <option>Sniper Rifle</option>
                                    <option>Fusion Rifle</option>
                                    <option>Shotgun</option>
                                    <option>Sidearm</option>
                                    <option>Machine Gun</option>
                                </select>
                            </td>
                        <?php }

                        else if ($coln[$counter] == "weaponRarity") { ?>
                            <td>
                            <select class="form-control" type="text" name="weaponRarity" required>
                                    <option>Basic</option>
                                    <option>Uncommon</option>
                                    <option>Rare</option>
                                    <option>Legendary</option>
                                    <option>Exotic</option>
                                </select>
                            </td>
                        <?php }
                        
                        else if ($coln[$counter] == "weaponSlotName") { ?>
                            <td>
                                <select class="form-control" type="text" name="weaponSlotName" required>
                                    <option>Primary</option>
                                    <option>Special</option>
                                    <option>Heavy</option>
                                </select>
                            </td>
                        <?php }

                        else {
                            ?>
                            <td>
                                <input class="form-control" type="text" value="<?php echo $row[$coln[$counter]] ?>"
                                       name="<?php echo $coln[$counter] ?>"/>
                            </td>
                            <?php
                        }
                    }
                    print "</tr>\n";
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php if ($query->rowCount() == 0) { ?>
            Weapon not found
        <?php } ?>
        <div class="panel-footer">
            <div class="text-right">
                <a class="btn btn-danger" href="admin.php?control=edit">Back</a>
                <button type="submit" class="btn btn-success" name="submit" data-toggle="modal" data-target="#myModal" >Update</button>
            </div>
        </div>
        
        <?php
        if (isset($_POST["submit"])) {

            $imgPath = $_POST["imgPath"];
            $weaponNewName = $_POST["weaponName"];
            $weaponRarity = $_POST["weaponRarity"];
            $weaponSlotName = $_POST["weaponSlotName"];
            $weaponTypeName = $_POST["weaponTypeName"];
            $weaponNewYear = $_POST["weaponYear"];
            $source = $_POST["source"];

            $weaponRange = $_POST["weaponRange"];
            $stability = $_POST["stability"];
            $impact = $_POST["impact"];
            $reload = $_POST["reload"];

            $bodyTTK = $_POST["bodyTTK"];
            $critTTK = $_POST["critTTK"];
            $aimAssist = $_POST["aimAssist"];

            $sql = "UPDATE weapon
                    SET weaponName='$weaponNewName', weaponYear='$weaponNewYear', source='$source',`weaponTypeName`='$weaponTypeName',`weaponSlotName`='$weaponSlotName',`weaponRarity`= '$weaponRarity',`imgPath`= '$imgPath'
                    WHERE weaponName = '$weaponName' AND weaponYear = $weaponYear";

            echo $sql;
            $query = $db->prepare($sql);
            if ($query->execute()) {
                echo "Pass";
            } else {
                echo "update fail";
            }

            $sql = "UPDATE `baseStat` 
                    SET `weaponRange`='$weaponRange',`stability`='$stability',`impact`='$impact',`reload`='$reload' 
                    WHERE weaponName = '$weaponNewName'";

            echo $sql;
            $query = $db->prepare($sql);
            if ($query->execute()) {
                echo "Pass";
            } else {
                echo "update fail";
            }

            $sql = "UPDATE `crucibleStat` 
                    SET `bodyTTK`='$bodyTTK',`critTTK`='$critTTK',`aimAssist`='$aimAssist'
                    WHERE weaponName = '$weaponNewName'";

            $query = $db->prepare($sql);
            if ($query->execute()) {
                echo "Pass";
            } else {
                echo "update fail";
            }

            } else {
            echo "No!";
        }


        ?>

    </div>
    </form>
</div>
</body>
