<?php
/**
 * Created by PhpStorm.
 * User: Bren
 * Date: 4/15/2016
 * Time: 11:48 PM
 */
include("../html/main.html");
include("../php/destiny_shared.php");

$top = "";
if ( isset($_GET["top"])) {
    $top = $_GET["top"];
}

if ($top == "aimassist") {
    $sql = "SELECT baseStat.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
            FROM baseStat
            INNER JOIN weapon 
            ON baseStat.weaponName = weapon.weaponName
            INNER JOIN crucibleStat
            ON baseStat.weaponName = crucibleStat.weaponName
            ORDER BY crucibleStat.aimAssist DESC
            LIMIT 5";
}

else if ($top == "bodyttk") {
    $sql = "SELECT baseStat.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
            FROM baseStat
            INNER JOIN weapon
            ON baseStat.weaponName = weapon.weaponName 
            INNER JOIN crucibleStat
            ON baseStat.weaponName = crucibleStat.weaponName
            WHERE crucibleStat.bodyTTK IS NOT NULL
            ORDER BY crucibleStat.bodyTTK ASC
            LIMIT 5";
}

else if ($top == "critttk") {
    $sql = "SELECT baseStat.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
            FROM baseStat
            INNER JOIN weapon
            ON baseStat.weaponName = weapon.weaponName 
            INNER JOIN crucibleStat
            ON baseStat.weaponName = crucibleStat.weaponName
            WHERE crucibleStat.critTTK IS NOT NULL
            ORDER BY crucibleStat.critTTK ASC
            LIMIT 5";
}

else {
    $sql = "SELECT baseStat.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
            FROM baseStat
            INNER JOIN weapon 
            ON baseStat.weaponName = weapon.weaponName
            INNER JOIN crucibleStat
            ON baseStat.weaponName = crucibleStat.weaponName";
}
echo $sql;
$query = $db->prepare($sql);
$query->execute();
?>

<?php
$total = $query->columnCount();
for($counter = 0; $counter<$total; $counter++){
    $meta = $query->getColumnMeta($counter);
    $coln[$counter] = $meta['name'];
}
?>

<body>
    <div class="panel">
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <a type="button" href="?top=aimassist" class="btn btn-default">Top 5 Aim Assisted Weapons</a>
            <a type="button" href="?top=bodyttk" class="btn btn-default">Top 5 Body TTK</a>
            <a type="button" href="?top=critttk" class="btn btn-default">Top 5 Crit TTK</a>
        </div>
<!--        <div class="panel panel-default">-->
            <table class="table ">
                <thead>
                <tr>
                    <th>Weapon Name</th>
                    <th>Weapon Type</th>
                    <th>Year</th>
                    <th>Source</th>
                    <th>Weapon Range</th>
                    <th>Stability</th>
                    <th>Impact</th>
                    <th>Aim Assist</th>
                    <th>Reload Speed</th>
                    <th>Body TTK</th>
                    <th>Crit TTK</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $rows = $query->fetchAll();
                foreach($rows as $row) {
                    print "<tr>\n";
                    for($counter = 0; $counter<$total; $counter++){
                        if ($row[$coln[$counter]] == null) {
                            print "<td>N/A</td>\n";
                        }
                        print "<td>{$row[$coln[$counter]]}</td>\n";
                    }
                    print "</tr>\n";
                }
                ?>
                </tbody>
            </table>
<!--        </div>-->
    </div>
</body>
