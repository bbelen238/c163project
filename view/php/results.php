<?php
/**
 * Created by PhpStorm.
 * User: brenbln
 * Date: 4/28/16
 * Time: 3:06 PM
 */

include("../html/main.html");
include("../php/destiny_shared.php");

$weaponName = $_GET["weaponname"];
$weaponType = $_GET["weapontype"];
$weaponRarity = $_GET["weaponrarity"];
$minBodyTTK = $_GET["minBodyTTK"];
$maxBodyTTK = $_GET["maxBodyTTK"];
$minCritTTK = $_GET["minCritTTK"];
$maxCritTTK = $_GET["maxCritTTK"];
$minAimAssist = $_GET["minAimassist"];
$maxAimAssist = $_GET["maxAimassist"];
$minMagSize = $_GET["minMagSize"];
$maxMagSize = $_GET["maxMagSize"];

$weaponTypes = implode('","', $weaponType);
$weaponRarities = implode('","', $weaponRarity);


// A lot of if statements...

if ($weaponName == "") {
    $weaponNameQuery = "";
} else {
    $weaponNameQuery = " weapon.weaponName LIKE %" . $weaponName . "%";
}

if ($weaponTypes[0] == "Any") {
    $weaponTypeQuery = "";
} else {
    $weaponTypeQuery = ' weapon.weaponTypeName IN ("'. $weaponTypes . '")';
}

if ($weaponRarities[0] == "Any") {
    $weaponRarityQuery = "";
} else {
    $weaponRarityQuery = ' AND weapon.weaponRarity IN ("'. $weaponRarities . '")';
}

if ($minBodyTTK == "") {
    $minBodyTTKQuery = "";
} else {
    $minBodyTTKQuery = ' AND cruciblestat.bodyTTK > ' . $minBodyTTK;
}

if ($maxBodyTTK == "") {
    $maxBodyTTKQuery = "";
} else {
    $maxBodyTTKQuery = ' AND cruciblestat.bodyTTK < ' . $maxBodyTTK;
}

if ($minCritTTK == "") {
    $minCritTTKQuery = "";
} else {
    $minCritTTKQuery = ' AND cruciblestat.critTTK > ' . $minCritTTK;
}

if ($maxCritTTK == "") {
    $maxCritTTKQuery = "";
} else {
    $maxCritTTKQuery = ' AND cruciblestat.critTTK < ' . $maxCritTTK;
}

if ($minAimAssist == "") {
    $minAimAssistQuery = "";
} else {
    $minAimAssistQuery = ' AND cruciblestat.aimassist > ' . $minAimAssist;
}

if ($maxAimAssist == "") {
    $maxAimAssistQuery = "";
} else {
    $maxAimAssistQuery = ' AND cruciblestat.aimassist < ' . $maxAimAssist;
}


$sql = "SELECT weapon.imgPath, baseStat.weaponName, weapon.weaponTypeName, weapon.weaponSlotName, weapon.weaponRarity, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
        FROM baseStat 
        INNER JOIN weapon 
        ON baseStat.weaponName = weapon.weaponName
        INNER JOIN crucibleStat
        ON baseStat.weaponName = crucibleStat.weaponName
        WHERE" . $weaponTypeQuery . $weaponRarityQuery . $minBodyTTKQuery . $maxBodyTTKQuery . $minCritTTKQuery . $maxCritTTKQuery . $minAimAssistQuery . $maxAimAssistQuery;

echo $sql;
$query = $db->prepare($sql);
$query->execute();
$coln = null;
$total = $query->columnCount();
for ($counter = 0; $counter < $total; $counter++) {
    $meta = $query->getColumnMeta($counter);
    $coln[$counter] = $meta['name'];
}

?>
<body>
    <div id="main-window" class="panel panel-default">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Icon</th>
                <th>Weapon Name</th>
                <th>Weapon Type</th>
                <th>Year</th>
                <th>Source</th>
                <th>Weapon Range</th>
                <th>Stability</th>
                <th>Impact</th>
                <th>Aim Assist</th>
                <th>Reload Speed</th>
                <th>BodyTTK</th>
                <th>CritTTK</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $rows = $query->fetchAll();
    foreach($rows as $row) {
        print "<tr>\n";
        for($counter = 0; $counter<$total; $counter++){
            if ($counter == 0) {
                ?> <td>
                    <a href="../php/wpinfo.php?weapon=<?php echo$row[$coln[1]]?>" >
                        <img src="<?php echo $row[$coln[$counter]]?>" class="img-rounded" height="64px" width="64px" </img>
                    </a>
                </td> <?php
            } else {
                print "<td>{$row[$coln[$counter]]}</td>\n";
            }

        }
        print "</tr>\n";
    }
    ?>
    </tbody>
    </table>
    </div>
</body>
