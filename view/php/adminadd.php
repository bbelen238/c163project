<?php
/**
 * Created by PhpStorm.
 * User: brenbln
 * Date: 4/27/16
 * Time: 5:42 PM
 */

include("../html/main.html");
include("../php/destiny_shared.php");

$weaponType = str_replace("+", " ",$_GET["weapontype"]);
?>

<body>
    <div class="container-fluid" >
        <div class="panel" >

<?php
if ($weaponType == "Rocket Launcher") {
    ?>
    <form role="form" method="post">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add a weapon</h3>
                    </div>
                    <div class="panel panel-body">
                        <div class="form-group">
                            <label>Enter weapon name</label>
                            <input class="form-control" type="text" name="name" required/>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Year</label>
                                <input class="form-control" type="text" name="year" required/>
                                <label>Source</label>
                                <input class="form-control" type="text" name="source" required/>
                                <label>Blast Radius</label>
                                <input class="form-control" type="text" name="bradius" required/>
                                <label>Stability</label>
                                <input class="form-control" type="text" name="stability" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Velocity</label>
                                <input class="form-control" type="text" name="velocity" required/>
                                <label>Aim Assist</label>
                                <input class="form-control" type="text" name="aimassist" required/>
                                <label>Reload Speed</label>
                                <input class="form-control" type="text" name="reloadspeed" required/>
                                <label>Weapon Icon</label>
                                <input class="form-control" type="text" name="iconurl"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Capacity</label>
                                <input class="form-control" type="text" name="capacity"/>
                                <label>Weapon Slot</label>
                                <select class="form-control" type="text" name="slot" required>
                                    <option>Primary</option>
                                    <option>Special</option>
                                    <option>Heavy</option>
                                </select>
                                <label>Rarity</label>
                                <select class="form-control" type="text" name="rarity" required>
                                    <option>Basic</option>
                                    <option>Uncommon</option>
                                    <option>Rare</option>
                                    <option>Legendary</option>
                                    <option>Exotic</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            <!--                                  <button type="button" class="btn btn-danger">Cancel</button>-->
                            <button type="submit" name="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </div>

        <?php
        if (isset($_POST["submit"], $_POST["name"], $_POST["year"], $_POST["source"], $_POST["bradius"], $_POST["stability"], $_POST["velocity"], $_POST["aimassist"], $_POST["reloadspeed"], $_POST["slot"], $_POST["rarity"], $_POST["capacity"])) {

            $weaponName = $_POST["name"];
            $year = $_POST["year"];
            $source = $_POST["source"];
            $capacity = $_POST["capacity"];
            $bradius = $_POST["bradius"];
            $stability = $_POST["stability"];
            $velocity = $_POST["velocity"];
            $aimassist = $_POST["aimassist"];
            $reloadspeed = $_POST["reloadspeed"];
            $slot = $_POST["slot"];
            $rarity = $_POST["rarity"];

            if ($_POST["iconurl"] == null) {
                $iconurl = "../../images/placeholder.jpg";
            } else {
                $iconurl = $_POST["ironurl"];
            }

            $sql = "INSERT INTO DESTINYWEAPON.weapon(weaponName, weaponYear, source, weaponTypeName, weaponSlotName, weaponRarity, imgPath)
                              VALUES('$weaponName', '$year', '$source', '$weaponType', '$slot', '$rarity', '$iconurl')";
            $query = $db->prepare($sql);
            if ($query->execute()) {
                echo " \n Weapon insert success \n";
            } else {
                echo " \n Weapon insert fail \n";
            };

            $sql = "INSERT INTO DESTINYWEAPON.rocketLauncherStat(weaponName, velocity, blastRadius, stability, capacity, reload, aimassist) 
                              VALUES('$weaponName', '$velocity', '$bradius', '$stability', '$capacity', '$reloadspeed', '$aimassist')";
            $query = $db->prepare($sql);
            if ($query->execute()) {
                echo "Rocket stat insert success \n";
            } else {
                echo "Rocket stat insert fail \n";
            };
        }
        }

else if ($weaponType == "Sniper Rifle") { ?>
            <form role="form" method="post">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Add a weapon</h3>
                    </div>
                    <div class="panel panel-body">
                        <div class="form-group">
                            <label>Enter weapon name</label>
                            <input class="form-control" type="text" name="name" required/>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Year</label>
                                <input class="form-control" type="text" name="year" required/>
                                <label>Source</label>
                                <input class="form-control" type="text" name="source" required/>
                                <label>Weapon Range</label>
                                <input class="form-control" type="text" name="range" required/>
                                <label>Stability</label>
                                <input class="form-control" type="text" name="stability" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Impact</label>
                                <input class="form-control" type="text" name="impact" required/>
                                <label>Aim Assist</label>
                                <input class="form-control" type="text" name="aimassist" required/>
                                <label>Reload Speed</label>
                                <input class="form-control" type="text" name="reloadspeed" required/>
                                <label>Weapon Icon</label>
                                <input class="form-control" type="text" name="iconurl"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <label>Body DMG</label>
                                <input class="form-control" type="text" name="bodydmg" required/>
                                <label>Crit DMG</label>
                                <input class="form-control" type="text" name="critdmg" required/>
                                <label>Weapon Slot</label>
                                <select class="form-control" type="text" name="slot" required>
                                    <option>Primary</option>
                                    <option>Special</option>
                                    <option>Heavy</option>
                                </select>
                                <label>Rarity</label>
                                <select class="form-control" type="text" name="rarity" required>
                                    <option>Basic</option>
                                    <option>Uncommon</option>
                                    <option>Rare</option>
                                    <option>Legendary</option>
                                    <option>Exotic</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            <!--                                  <button type="button" class="btn btn-danger">Cancel</button>-->
                            <button type="submit" name="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </div>

                <?php
                if (isset($_POST["submit"], $_POST["name"], $_POST["year"], $_POST["source"], $_POST["range"], $_POST["stability"], $_POST["impact"], $_POST["aimassist"], $_POST["reloadspeed"], $_POST["bodydmg"],
                    $_POST["critdmg"], $_POST["slot"], $_POST["rarity"])) {

                    $weaponName = $_POST["name"];
                    $year = $_POST["year"];
                    $source = $_POST["source"];
                    $range = $_POST["range"];
                    $stability = $_POST["stability"];
                    $impact = $_POST["impact"];
                    $aimassist = $_POST["aimassist"];
                    $reloadspeed = $_POST["reloadspeed"];
                    $bodydmg = $_POST["bodydmg"];
                    $critdmg = $_POST["critdmg"];
                    $slot = $_POST["slot"];
                    $rarity = $_POST["rarity"];

                    if ($_POST["iconurl"] == null) {
                        $iconurl = "../../images/placeholder.jpg";
                    } else {
                        $iconurl = $_POST["ironurl"];
                    }

                    $sql = "INSERT INTO DESTINYWEAPON.weapon(weaponName, weaponYear, source, weaponTypeName, weaponSlotName, weaponRarity, imgPath)
                              VALUES('$weaponName', '$year', '$source', '$weaponType', '$slot', '$rarity', '$iconurl')";
                    $query = $db->prepare($sql);
                    if ($query->execute()) {
                        echo " \n Weapon insert success \n";
                    } else {
                        echo " \n Weapon insert fail \n";
                    };

                    $sql = "INSERT INTO DESTINYWEAPON.baseStat(weaponName, weaponRange, stability, impact, reload)
                              VALUES ('$weaponName', '$range', '$stability', '$impact', '$reloadspeed')";
                    $query = $db->prepare($sql);
                    if ($query->execute()) {
                        echo "Weapon basestat insert success \n";
                    } else {
                        echo "Weapon basestat insert fail \n";
                    };

                    $sql = "INSERT INTO DESTINYWEAPON.crucibleStat(weaponName, bodyTTK, critTTK, aimAssist)
                              VALUES ('$weaponName', '$bodydmg', '$critdmg','$aimassist')";
                    $query = $db->prepare($sql);
                    if ($query->execute()) {
                        echo "Weapon cruciblestat insert success \n";
                    } else {
                        echo "Weapon cruciblestat insert fail \n";
                    };
                }
                }

else { ?>

        <form role="form" method="post" >
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Add a weapon</h3>
        </div>
        <div class="panel panel-body">
            <div class="form-group">
                <label>Enter weapon name</label>
                <input class="form-control" type="text" name="name" required/>
            </div>
            <div class="form-group">
                <div class="col-lg-2">
                    <label>Year</label>
                    <input class="form-control" type="text" name="year" required/>
                    <label>Source</label>
                    <input class="form-control" type="text" name="source" required/>
                    <label>Weapon Range</label>
                    <input class="form-control" type="text" name="range" required/>
                    <label>Stability</label>
                    <input class="form-control" type="text" name="stability" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-2">
                    <label>Impact</label>
                    <input class="form-control" type="text" name="impact" required/>
                    <label>Aim Assist</label>
                    <input class="form-control" type="text" name="aimassist" required/>
                    <label>Reload Speed</label>
                    <input class="form-control" type="text" name="reloadspeed" required/>
                    <label>Weapon Icon</label>
                    <input class="form-control" type="text" name="iconurl"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-2">
                    <label>Body TTK</label>
                    <input class="form-control" type="text" name="bodyttk" required/>
                    <label>Crit TTK</label>
                    <input class="form-control" type="text" name="critttk" required/>
                    <label>Weapon Slot</label>
                    <select class="form-control" type="text" name="slot" required>
                        <option>Primary</option>
                        <option>Special</option>
                        <option>Heavy</option>
                    </select>
                    <label>Rarity</label>
                    <select class="form-control" type="text" name="rarity" required>
                        <option>Basic</option>
                        <option>Uncommon</option>
                        <option>Rare</option>
                        <option>Legendary</option>
                        <option>Exotic</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="text-right">
                <!--                                  <button type="button" class="btn btn-danger">Cancel</button>-->
                <button type="submit" name="submit" class="btn btn-success">Add</button>
            </div>
        </div>
    </div>

    <?php
    if (isset($_POST["submit"], $_POST["name"], $_POST["year"], $_POST["source"], $_POST["range"], $_POST["stability"], $_POST["impact"], $_POST["aimassist"], $_POST["reloadspeed"], $_POST["bodyttk"],
        $_POST["critttk"], $_POST["slot"], $_POST["rarity"])) {

        $weaponName = $_POST["name"];
        $year = $_POST["year"];
        $source = $_POST["source"];
        $range = $_POST["range"];
        $stability = $_POST["stability"];
        $impact = $_POST["impact"];
        $aimassist = $_POST["aimassist"];
        $reloadspeed = $_POST["reloadspeed"];
        $bodyttk = $_POST["bodyttk"];
        $critttk = $_POST["critttk"];
        $slot = $_POST["slot"];
        $rarity = $_POST["rarity"];

        if ($_POST["iconurl"] == null) {
            $iconurl = "../../images/placeholder.jpg";
        } else {
            $iconurl = $_POST["ironurl"];
        }
        

                      $sql = "INSERT INTO DESTINYWEAPON.weapon(weaponName, weaponYear, source, weaponTypeName, weaponSlotName, weaponRarity, imgPath)
                              VALUES('$weaponName', '$year', '$source', '$weaponType', '$slot', '$rarity', '$iconurl')";
                      $query = $db->prepare($sql);
                      if ($query->execute()) {
                          echo " \n Weapon insert success \n";
                      } else {
                          echo " \n Weapon insert fail \n";
                      };

                      $sql = "INSERT INTO DESTINYWEAPON.baseStat(weaponName, weaponRange, stability, impact, reload)
                              VALUES ('$weaponName', '$range', '$stability', '$impact', '$reloadspeed')";
                      $query = $db->prepare($sql);
                      if ($query->execute()) {
                          echo "Weapon basestat insert success \n";
                      } else {
                          echo "Weapon basestat insert fail \n";
                      };

                      $sql = "INSERT INTO DESTINYWEAPON.crucibleStat(weaponName, bodyTTK, critTTK, aimAssist)
                              VALUES ('$weaponName', '$bodyttk', '$critttk','$aimassist')";
                      $query = $db->prepare($sql);
                      if ($query->execute()) {
                          echo "Weapon cruciblestat insert success \n";
                      } else {
                          echo "Weapon cruciblestat insert fail \n";
                      };

     }
} ?>
            </form>
        </div>
    </div>
</body>

