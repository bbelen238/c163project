<?php
/**
 * Created by PhpStorm.
 * User: brenbln
 * Date: 4/28/16
 * Time: 8:01 AM
 */

include("../html/main.html");
include("../php/destiny_shared.php");

?>

<body>
    <div class="panel">
        <form role="form" action="results.php" method="get">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Refine search</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Select weapon type</label>
                        <select multiple class="form-control"  name="weapontype[]" >
                            <option>Any</option>
                            <option>Auto Rifle</option>
                            <option>Scout Rifle</option>
                            <option>Pulse Rifle</option>
                            <option>Hand Cannon</option>
                            <option>Sniper Rifle</option>
                            <option>Fusion Rifle</option>
                            <option>Shotgun</option>
                            <option>Sidearm</option>
                            <option>Rocket Launcher</option>
                            <option>Machine Gun</option>
                        </select>
                        <label>Select weapon rarity</label>
                        <select multiple class="form-control" name="weaponrarity[]" >
                            <option>Any</option>
                            <option>Basic</option>
                            <option>Uncommon</option>
                            <option>Rare</option>
                            <option>Legendary</option>
                            <option>Exotic</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2">
                        <label>Body TTK Range</label>
                            <input type="text" class="form-control" placeholder="min" name="minBodyTTK">
                            to
                            <input type="text" class="form-control" placeholder="max" name="maxBodyTTK">
                        </div>

                        <div class="col-lg-2">
                            <label>Crit TTK Range</label>
                            <input type="text" class="form-control" placeholder="min" name="minCritTTK">
                            to
                            <input type="text" class="form-control" placeholder="max" name="maxCritTTK">
                        </div>

                        <div class="col-lg-2">
                            <label>Aim Assist</label>
                            <input type="text" class="form-control" placeholder="min" name="minAimassist">
                            to
                            <input type="text" class="form-control" placeholder="max" name="maxAimassist">
                        </div>

                        <div class="col-lg-2">
                            <label>Capacity (Rocket Launcher)</label>
                            <input type="text" class="form-control" placeholder="min" name="minMagSize">
                            to
                            <input type="text" class="form-control" placeholder="max" name="maxMagSize">
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
