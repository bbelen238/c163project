<?php
/**
 * Created by PhpStorm.
 * User: brenbln
 * Date: 4/23/16
 * Time: 10:04 PM
 */

include("../html/main.html");
include("../php/destiny_shared.php");

$controlType = $_GET["control"];
//echo $controlType;
?>

<body>
    <div class="container-fluid">
        <div class="panel">
            <?php
              if ($controlType == "edit") { ?>
                  <form role="form" action="adminedit.php" method="get">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h3>Update a weapon</h3>
                          </div>
                          <div class="panel-body" >
                              <div class="form-group" >
                                  <div class="col-lg-2" >
                                      <label>Enter weapon name</label>
                                      <input type="text" class="form-control" placeholder="Weapon name" name="name">
                                      <label>Enter weapon year</label>
                                      <input type="text" class="form-control" placeholder="Year" name= "year">
                                  </div>
                              </div>
                          </div>
                          <div class="panel-footer">
                              <button class="btn btn-success" type="submit">Search</button>
                          </div>
                      </div>
                  </form>
              <?php }
              if ($controlType == "add") { ?>
                  <form role="form" action="adminadd.php" method="get">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h3>Add a weapon</h3>
                          </div>
                          <div class="panel-body">
                              <div class="form-group">
                                  <label>Select weapon type</label>
                                  <select class="form-control" type="text" name="weapontype" >
                                      <option>Auto Rifle</option>
                                      <option>Scout Rifle</option>
                                      <option>Pulse Rifle</option>
                                      <option>Hand Cannon</option>
                                      <option>Sniper Rifle</option>
                                      <option>Fusion Rifle</option>
                                      <option>Shotgun</option>
                                      <option>Sidearm</option>
                                      <option>Rocket Launcher</option>
                                      <option>Machine Gun</option>
                                  </select>
                              </div>
                          </div>
                          <div class="panel-footer">
                              <div class="text-right">
                                  <button type="submit" class="btn btn-success">Add</button>
                              </div>
                          </div>
                      </div>
                  </form>
                  <?php }
              if ($controlType == "delete") { ?>
                  <form role="form" method="post">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3>Delete a weapon</h3>
                          </div>
                          <div class="panel-body" >
                               <div class="form-group" >
                                   <div class="col-lg-2" >
                                       <label>Enter weapon name</label>
                                       <input type="text" class="form-control" placeholder="Weapon name" name="name">
                                   </div>
                               </div>
                          </div>
                          <div class="panel-footer">
                              <button name="submit" class="btn btn-success" type="submit">Delete</button>
                          </div>
                      </div>
                  <?php
                    if (isset($_POST["submit"])) {
                        $weaponName = $_POST["name"];
                        $sql = "DELETE FROM weapon
                                WHERE weaponName = '$weaponName'";
                        $query = $db->prepare($sql);
                        if ($query->execute()) {
                            echo 'Success';
                        } else {
                            echo 'Fail';
                        }
                    }





?> </form> <?php


              } ?>
        </div>
    </div>
</body>