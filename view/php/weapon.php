<?php
/**
 * Created by PhpStorm.
 * User: Bren
 * Date: 4/16/2016
 * Time: 12:37 AM
 */
include("../html/main.html");
include("../php/destiny_shared.php");

$weaponTypeName = str_replace("+", " ", $_GET["weapon"]);

if ($weaponTypeName != "Rocket Launcher") {
    $sql = "SELECT weapon.imgPath, baseStat.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, baseStat.weaponRange, baseStat.stability, baseStat.impact, crucibleStat.aimAssist, baseStat.reload, crucibleStat.bodyTTK, crucibleStat.critTTK
    FROM baseStat 
    INNER JOIN weapon 
    ON baseStat.weaponName = weapon.weaponName
    INNER JOIN crucibleStat
    ON baseStat.weaponName = crucibleStat.weaponName
    WHERE weapon.weaponTypeName = '". $weaponTypeName . "'";
    echo $sql;
    $query = $db->prepare($sql);
    $query->execute();
    ?>

    <?php
    $total = $query->columnCount();
    for($counter = 0; $counter<$total; $counter++){
        $meta = $query->getColumnMeta($counter);
        $coln[$counter] = $meta['name'];
    }
    ?>

    <body>
    <div id="main-window" class="panel panel-default">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Icon</th>
                <th>Weapon Name</th>
                <th>Weapon Type</th>
                <th>Year</th>
                <th>Source</th>
                <th>Weapon Range</th>
                <th>Stability</th>
                <th>Impact</th>
                <th>Aim Assist</th>
                <th>Reload Speed</th>
                <?php
                if ($weaponTypeName == "Sniper Rifle") {
                    ?>
                    <th>BodyDMG</th>
                    <th>CritDMG</th>
                <?php } else { ?>
                        <th>BodyTTK</th>
                        <th>CritTTK</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php
            $rows = $query->fetchAll();
            foreach($rows as $row) {
                print "<tr>\n";
                for($counter = 0; $counter<$total; $counter++){
                    if ($counter == 0) {
                        ?> <td>
                            <a href="../php/wpinfo.php?weapon=<?php echo$row[$coln[1]]?>" >
                            <img src="<?php echo $row[$coln[$counter]]?>" class="img-rounded" height="64px" width="64px" </img>
                            </a>
                        </td> <?php
                    } else {
                        print "<td>{$row[$coln[$counter]]}</td>\n";
                    }

                }
                print "</tr>\n";
            }
            ?>
            </tbody>
        </table>
    </div>
    </body>
    <?php
}

else {
    $sql = "SELECT weapon.imgPath, weapon.weaponName, weapon.weaponTypeName, weapon.weaponYear, weapon.source, rocketLauncherStat.blastRadius, rocketLauncherStat.capacity, rocketLauncherStat.velocity, rocketLauncherStat.stability, rocketLauncherStat.reload, rocketLauncherStat.aimassist
        FROM rocketLauncherStat 
        INNER JOIN weapon 
        ON rocketLauncherStat.weaponName = weapon.weaponName
        WHERE weapon.weaponTypeName = '" . $weaponTypeName . "'";
    echo $sql;
    $query = $db->prepare($sql);
    $query->execute();
    ?>

    <?php
    $total = $query->columnCount();
    for ($counter = 0; $counter < $total; $counter++) {
        $meta = $query->getColumnMeta($counter);
        $coln[$counter] = $meta['name'];
    }
    ?>

    <body>
    <div id="main-window" class="panel panel-default">
    <table class="table table-bordered">
    <thead>
    <tr>
        <th>Icon</th>
        <th>Weapon Name</th>
        <th>Weapon Type</th>
        <th>Year</th>
        <th>Source</th>
        <th>Blast Radius</th>
        <th>Capacity</th>
        <th>Velocity</th>
        <th>Stability</th>
        <th>Reload Speed</th>
        <th>Aim Assist</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $rows = $query->fetchAll();
    foreach ($rows as $row) {
        print "<tr>\n";
        for ($counter = 0; $counter < $total; $counter++) {
            if ($counter == 0) {
                ?>
                <td>
                    <a href="../php/wpinfo.php?weapon=<?php echo$row[$coln[1]]?>" >
                    <img src="<?php echo $row[$coln[$counter]] ?>" class="img-rounded" height="64px" width="64px" </img>
                    </a>
                </td> <?php
            } else {
                print "<td>{$row[$coln[$counter]]}</td>\n";
            }

        }
        print "</tr>\n";
    }

            ?>
            </tbody>
        </table>
    </div>
    </body>
<?php
}
?>